﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL;
using System.Net.Mail;
namespace BLL
{
    public class TeacherBLL
    {
        DBDataContext db = new DBDataContext();

        public List<string> GetTeacherRoles()
        {
            return db.Roles.Where(x => x.RoleName != "Admin" && x.RoleName!="Sinh viên").Select(x=>x.RoleName).ToList();
        }
        public object ListTeacher()
        {
            var teacherList = (from u in db.Users
                                      join f in db.Faculties on u.FacultyID equals f.FacultyID
                                      join r in db.Roles on u.RoleID equals r.RoleID
                                      select new
                                      {
                                          u.UserID,
                                          u.UserName,
                                          u.Email,
                                          f.FacultyName,
                                          r.RoleName,
                                          u.Locked
                                      }).ToList();

            return teacherList.Where(x=>x.RoleName=="Giảng viên").ToList();
        }
        /// <summary>
        /// Thêm mới một tài khoản Giảng viên
        /// </summary>
        /// <param name="name">Họ tên người dùng</param>
        /// <param name="email">Địa chỉ mail là duy nhất</param>
        /// <param name="password">Mật khẩu</param>
        /// <param name="role">Mã quyền</param>
        /// <param name="facultyID">Mã khoa</param>
        /// <param name="image">Hình ảnh, đường dẫn đến file ảnh</param>
        /// <returns>Một đối tượng tài khoản</returns>
        /// 
        public bool checkExist(string email)
        {
            return db.Users.Where(x => x.Email == email).FirstOrDefault() != null ? true : false;
        }
        public bool Insert(string name, string email, string password, byte facultyID)
        {
            bool existed = checkExist(email);
            bool inserted = false;
            try
            {
                if (!existed)
                {
                    User teacher = new User();
                    teacher.Email = email;
                    teacher.Password = password;
                    teacher.UserName = name;
                    teacher.FacultyID = facultyID;
                    teacher.RoleID = 2;
                    db.Users.InsertOnSubmit(teacher);
                    db.SubmitChanges();
                    inserted = true;
                }
                return inserted;
            }
            catch
            {
                return inserted;
            }


        }
    }
}
