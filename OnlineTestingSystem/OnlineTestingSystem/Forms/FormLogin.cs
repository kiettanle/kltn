﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OnlineTestingSystem.Forms
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            //Hàm khởi tạo các thành phần có trong giao diện như Button, Label ...
            InitializeComponent();
            txtEmail.Focus();
        }
        
        //Xử lý nút Login trên FormLogin
        private void btnLogin_Click(object sender, EventArgs e)
        {
            if(string.IsNullOrEmpty(txtEmail.Text))
                lblError.Text = "Đăng nhập thất bại!";
            else
            {
                //Đăng nhập thành công
                //FormMain.Logged = true;
                this.Close();
            }
            //if (CheckPassword(txtPassword.Text))
            //    lblError.Text += "\n Password can't contain special character !";
        }

        //Xử lý nút Cancel trên FormLogin
        private void btnCancel_Click(object sender, EventArgs e)
        {
            //Đóng FormLogin
            this.Close();
        }
        //Hàm kiểm tra Email hợp lệ
        bool CheckMail(string email)
        {
            //Email có chứa định dạng *@hcmute.edu.vn
            if (email.Contains("@hcmute.edu.vn"))
                return true;
            return false;
        }
        //Hàm kiểm tra mật khẩu hợp lệ
        bool CheckPassword(string password)
        {
            if (password.Contains("'--orand"))
                return false;
            return true;
        }
    }
}
