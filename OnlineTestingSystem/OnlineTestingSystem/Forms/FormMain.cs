﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraEditors;
using System.Drawing.Drawing2D;

namespace OnlineTestingSystem.Forms
{
    public partial class FormMain : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        int focus = 1;
        int horizotal = 10;
        int vertical = 30;
        GaryPerkin.UserControls.Buttons.RoundButton[] buttonArray = new GaryPerkin.UserControls.Buttons.RoundButton[300];
        public FormMain()
        {
            InitializeComponent();
        }
        private void FormMain_Load(object sender, EventArgs e)
        {
            for (int i = 1; i <= 200; i++)
            {
                buttonArray[i] = new GaryPerkin.UserControls.Buttons.RoundButton();
                buttonArray[i].Tag = i.ToString();
                buttonArray[i].Text = i.ToString();
                buttonArray[i].Size = new Size(40, 30);
                buttonArray[i].Click += new EventHandler(btn_Click);
                buttonArray[i].MouseUp += new MouseEventHandler(right_Click);
                if (xtraScrollableControl1.Width > horizotal + 100)
                {
                    horizotal += 40;
                }

                else
                {
                    vertical += 30;
                    horizotal = 10;
                }
                buttonArray[i].Location = new Point(horizotal, vertical);
                //groupControlListQuestion.Controls.Add(buttonArray[i]);
                //panelQuestion.Controls.Add(buttonArray[i]);
                xtraScrollableControl1.Controls.Add(buttonArray[i]);
            }
            buttonArray[1].Focus();
        }
        private void btnPrevious_Click(object sender, EventArgs e)
        {
            bool flag = false;
            for (int i = 200; i > 0; i--)
            {
                if (Convert.ToInt32(buttonArray[i].Tag.ToString()) == focus)
                {
                    if (focus > 1)
                    {
                        buttonArray[i - 1].Focus();
                        if (buttonArray[i - 1].BackColor != Color.Green)
                            buttonArray[i - 1].BackColor = Color.Gray;
                        buttonArray[i - 1].ForeColor = Color.White;
                        flag = true;
                    }
                }
            }
            if (flag)
                focus--;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            buttonArray[focus].BackColor = Color.Green;
            focus++;
            buttonArray[focus].Focus();
            buttonArray[focus].ForeColor = Color.White;
            if (buttonArray[focus].BackColor != Color.Green)
                buttonArray[focus].BackColor = Color.Gray;
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            bool flag = false;
            for (int i = 1; i <= 200; i++)
            {
                if (Convert.ToInt32(buttonArray[i].Tag.ToString()) == focus)
                {
                    if (focus < 200)
                    {
                        buttonArray[i + 1].Focus();
                        buttonArray[i + 1].ForeColor = Color.White;
                        if (buttonArray[i + 1].BackColor != Color.Green)
                            buttonArray[i + 1].BackColor = Color.Gray;
                        flag = true;
                    }
                }
            }
            if (flag)
                focus++;
        }
        private void btn_Click(object sender, EventArgs e)
        {
            GaryPerkin.UserControls.Buttons.RoundButton btn = (GaryPerkin.UserControls.Buttons.RoundButton)sender;
            if (btn.BackColor != Color.Green)
                btn.BackColor = Color.Gray;
            btn.ForeColor = Color.White;
            focus = Convert.ToInt32(btn.Tag.ToString());
        }

        private void right_Click(object sender, MouseEventArgs e)
        {
            GaryPerkin.UserControls.Buttons.RoundButton btn = (GaryPerkin.UserControls.Buttons.RoundButton)sender;
            if (e.Button == MouseButtons.Right)
                btn.BackColor = Color.Red;
            btn.ForeColor = Color.White;
        }

        private void btn_Hover(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            btn.BackColor = Color.Azure;
            focus = Convert.ToInt32(btn.Tag.ToString());
        }
        private void btn_Leave(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            btn.BackColor = Color.FromArgb(255, 240, 240, 240);
            focus = Convert.ToInt32(btn.Tag.ToString());
        }
        int seconds = 1800;
        private void timer_Tick(object sender, EventArgs e)
        {
            progressBarTimer.Maximum = 1800;
            progressBarControlTimer.Properties.Maximum = 1800;
            if (seconds < 0)
            {
                timer.Stop();
                MessageBox.Show("Hết giờ làm bài", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Stop);
            }
            else
            {
                progressBarControlTimer.EditValue = seconds;
                progressBarControlTimer.Update();
                progressBarTimer.Value = seconds;
                if (seconds < 60)
                {
                    progressBarTimer.BackColor = Color.Red;
                    progressBarControlTimer.BackColor = Color.Red;
                }
                progressBarTimer.Update();
                labelSecond.Text = seconds / 60 + " : " + ((seconds % 60) >= 10 ? (seconds % 60).ToString() : "0" + seconds % 60);
                seconds--;
            }

        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            DialogResult dialog = MessageBox.Show("Bạn chắc chắn muốn thoát chương trình ?", "Thông báo !", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
            if (dialog == DialogResult.Yes)
                Application.Exit();
            else
                if (dialog == DialogResult.No)
                {
                    e.Cancel = true;
                    this.Activate();
                }
        }
    }
}