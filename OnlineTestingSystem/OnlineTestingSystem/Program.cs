﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using DevExpress.LookAndFeel;

namespace OnlineTestingSystem
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            DevExpress.Skins.SkinManager.EnableFormSkins();
            DevExpress.UserSkins.BonusSkins.Register();
            //Chọn Skin mặc định là McSkin
            UserLookAndFeel.Default.SetSkinStyle("Office 2007 Blue");

            Application.Run(new OnlineTestingSystem.Forms.FormMain());
        }
    }
}