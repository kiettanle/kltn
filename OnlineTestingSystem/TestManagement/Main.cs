﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraBars;

namespace TestManagement
{
    public partial class Main : DevExpress.XtraBars.Ribbon.RibbonForm
    {
        public Main()
        {
            InitializeComponent();
        }

        public void TabCreating(DevExpress.XtraTab.XtraTabControl TabControl, string Text, System.Windows.Forms.UserControl Form)
        {
            int Index = CheckExistTab(TabControl, Text);
            if (Index >= 0)
            {
                TabControl.SelectedTabPage = TabControl.TabPages[Index];
                TabControl.SelectedTabPage.Text = Text;
            }
            else
            {
                // DevExpress.XtraTab.XtraTabControl TabPage = new DevExpress.XtraTab.XtraTabControl();
                DevExpress.XtraTab.XtraTabPage TabPage = new DevExpress.XtraTab.XtraTabPage { Text = Text };
                TabControl.TabPages.Add(TabPage);
                TabControl.SelectedTabPage = TabPage;
                //Form.TopLevel = false;
                Form.Parent = TabPage;
                Form.Show();
                Form.Dock = DockStyle.Fill;
            }
        }
        public static int CheckExistTab(DevExpress.XtraTab.XtraTabControl TabControlName, string TabName)
        {
            int temp = -1;
            for (int i = 0; i < TabControlName.TabPages.Count; i++)
            {
                if (TabControlName.TabPages[i].Text == TabName)
                {
                    temp = i;
                    break;
                }
            }
            return temp;
        }


        private void btnExit_ItemClick(object sender, ItemClickEventArgs e)
        {

        }
        

        private void btnAccount_ItemClick(object sender, ItemClickEventArgs e)
        {
            int t = 0;
            foreach (DevExpress.XtraTab.XtraTabPage tab in xtraTabControl1.TabPages)
            {
                if (tab.Text == "Chi tiết tài khoản")
                {
                    xtraTabControl1.SelectedTabPage = tab;
                    t = 1;
                }
            }
            if (t == 1)
            {

            }
            else
            {
                TabCreating(xtraTabControl1, "Chi tiết tài khoản", new ucAccount());
            }
        }

        private void xtraTabControl1_CloseButtonClick(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage.Dispose();
        }

        private void btnTeacherList_ItemClick(object sender, ItemClickEventArgs e)
        {
            int t = 0;
            foreach (DevExpress.XtraTab.XtraTabPage tab in xtraTabControl1.TabPages)
            {
                if (tab.Text == "Danh sách tài khoản")
                {
                    xtraTabControl1.SelectedTabPage = tab;
                    t = 1;
                }
            }
            if (t == 1)
            {

            }
            else
            {
                TabCreating(xtraTabControl1, "Danh sách giảng viên", new ucTeacherList());
            }
        }

        private void btnImportTeacher_ItemClick(object sender, ItemClickEventArgs e)
        {

        }

        private void btnRoleList_ItemClick(object sender, ItemClickEventArgs e)
        {
            int t = 0;
            foreach (DevExpress.XtraTab.XtraTabPage tab in xtraTabControl1.TabPages)
            {
                if (tab.Text == "Danh sách quyền")
                {
                    xtraTabControl1.SelectedTabPage = tab;
                    t = 1;
                }
            }
            if (t == 1)
            {

            }
            else
            {
                TabCreating(xtraTabControl1, "Danh sách quyền", new ucRoleList());
            }
        }

        private void btnAddRole_ItemClick(object sender, ItemClickEventArgs e)
        {
            FormAddRole f = new FormAddRole();
            f.ShowDialog();
        }
    }
}