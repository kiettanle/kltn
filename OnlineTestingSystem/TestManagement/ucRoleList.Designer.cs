﻿namespace TestManagement
{
    partial class ucRoleList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.gcRole = new DevExpress.XtraGrid.GridControl();
            this.gvRole = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.gcRoleDetail = new DevExpress.XtraGrid.GridControl();
            this.gvRoleDetail = new DevExpress.XtraGrid.Views.Grid.GridView();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcRole)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRole)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gcRoleDetail)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRoleDetail)).BeginInit();
            this.SuspendLayout();
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.gcRole);
            this.groupControl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupControl1.Location = new System.Drawing.Point(0, 0);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(899, 222);
            this.groupControl1.TabIndex = 0;
            this.groupControl1.Text = "Quyền";
            // 
            // gcRole
            // 
            this.gcRole.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcRole.Location = new System.Drawing.Point(2, 21);
            this.gcRole.MainView = this.gvRole;
            this.gcRole.Name = "gcRole";
            this.gcRole.Size = new System.Drawing.Size(895, 199);
            this.gcRole.TabIndex = 0;
            this.gcRole.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvRole});
            this.gcRole.Load += new System.EventHandler(this.gcRole_Load);
            // 
            // gvRole
            // 
            this.gvRole.GridControl = this.gcRole;
            this.gvRole.Name = "gvRole";
            this.gvRole.OptionsView.ShowColumnHeaders = false;
            this.gvRole.OptionsView.ShowIndicator = false;
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.gcRoleDetail);
            this.groupControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControl2.Location = new System.Drawing.Point(0, 222);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(899, 301);
            this.groupControl2.TabIndex = 1;
            this.groupControl2.Text = "Chi tiết quyền";
            // 
            // gcRoleDetail
            // 
            this.gcRoleDetail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gcRoleDetail.Location = new System.Drawing.Point(2, 21);
            this.gcRoleDetail.MainView = this.gvRoleDetail;
            this.gcRoleDetail.Name = "gcRoleDetail";
            this.gcRoleDetail.Size = new System.Drawing.Size(895, 278);
            this.gcRoleDetail.TabIndex = 0;
            this.gcRoleDetail.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gvRoleDetail});
            // 
            // gvRoleDetail
            // 
            this.gvRoleDetail.GridControl = this.gcRoleDetail;
            this.gvRoleDetail.Name = "gvRoleDetail";
            // 
            // ucRoleList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Name = "ucRoleList";
            this.Size = new System.Drawing.Size(899, 523);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcRole)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRole)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gcRoleDetail)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gvRoleDetail)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraGrid.GridControl gcRole;
        private DevExpress.XtraGrid.Views.Grid.GridView gvRole;
        private DevExpress.XtraGrid.GridControl gcRoleDetail;
        private DevExpress.XtraGrid.Views.Grid.GridView gvRoleDetail;

    }
}
