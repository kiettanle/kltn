﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
namespace TestManagement
{
    public partial class ucRoleList : DevExpress.XtraEditors.XtraUserControl
    {
        StudentBLL student = new StudentBLL();
        public ucRoleList()
        {
            InitializeComponent();
        }

        private void gcRole_Load(object sender, EventArgs e)
        {
            gcRole.DataSource = student.ListRole();
        }
    }
}
