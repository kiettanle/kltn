﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BLL;
namespace TestManagement
{
    public partial class ucTeacherList : UserControl
    {
        TeacherBLL teacher = new TeacherBLL();
        public ucTeacherList()
        {
            InitializeComponent();
        }

        private void Reset()
        {
            txtEmail.ResetText();
            txtName.ResetText();
            txtPassword.ResetText();
            cbFaculty.ResetText();
        }
        private void LoadTeacherList(object sender, EventArgs e)
        {
            gCTeacherList.DataSource = teacher.ListTeacher();
            cbRole.DataSource = teacher.GetTeacherRoles();
        }
        bool IsNull()
        {
            if (cbFaculty.SelectedIndex.ToString() == "" || txtEmail.Text == "" || txtName.Text == "" || txtPassword.Text == "")
                return true;
            return false;
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                if (email.Contains("@hcmute.edu.vn"))
                    return true;
                return true;
            }
            catch
            {
                return false;
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsNull())
                {
                    MessageBox.Show("Vui lòng nhập đầy đủ các trường !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (!IsValidEmail(txtEmail.Text))
                {
                    MessageBox.Show("Email không hợp lệ !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (teacher.checkExist(txtEmail.Text))
                {
                    MessageBox.Show("Email đã được sử dụng !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                bool ok = teacher.Insert(txtName.Text, txtEmail.Text, txtPassword.Text, Convert.ToByte(cbFaculty.SelectedIndex));
                if (ok)
                {
                    MessageBox.Show("OK");
                    gCTeacherList.DataSource = teacher.ListTeacher();
                }
                else
                {
                    MessageBox.Show("Error");
                }
            }
            catch
            {
                MessageBox.Show("Thêm không thành công !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
        }

        private void gvTeacherList_Click(object sender, EventArgs e)
        {
            txtName.Text = "AAA";
        }

        private void gvTeacherList_RowClick(object sender, DevExpress.XtraGrid.Views.Grid.RowClickEventArgs e)
        {
            txtName.Text = gvTeacherList.GetRowCellValue(gvTeacherList.FocusedRowHandle, "UserName").ToString();
            txtEmail.Text = gvTeacherList.GetRowCellValue(gvTeacherList.FocusedRowHandle, "Email").ToString();
            txtPassword.Text = gvTeacherList.GetRowCellValue(gvTeacherList.FocusedRowHandle, "Password").ToString();
        }

        private void gvTeacherList_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            txtName.Text = gvTeacherList.GetRowCellValue(gvTeacherList.FocusedRowHandle, "UserName").ToString();
            txtEmail.Text = gvTeacherList.GetRowCellValue(gvTeacherList.FocusedRowHandle, "Email").ToString();
            txtPassword.Text = gvTeacherList.GetRowCellValue(gvTeacherList.FocusedRowHandle, "Password").ToString();
        }

        private void gvTeacherList_CellValueChanged(object sender, DevExpress.XtraGrid.Views.Base.CellValueChangedEventArgs e)
        {
            txtName.Text = gvTeacherList.GetRowCellValue(gvTeacherList.FocusedRowHandle, "UserName").ToString();
            txtEmail.Text = gvTeacherList.GetRowCellValue(gvTeacherList.FocusedRowHandle, "Email").ToString();
            txtPassword.Text = gvTeacherList.GetRowCellValue(gvTeacherList.FocusedRowHandle, "Password").ToString();
        }

        private void gvTeacherList_SelectionChanged(object sender, DevExpress.Data.SelectionChangedEventArgs e)
        {
            txtName.Text = gvTeacherList.GetRowCellValue(gvTeacherList.FocusedRowHandle, "UserName").ToString();
            txtEmail.Text = gvTeacherList.GetRowCellValue(gvTeacherList.FocusedRowHandle, "Email").ToString();
            txtPassword.Text = gvTeacherList.GetRowCellValue(gvTeacherList.FocusedRowHandle, "Password").ToString();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {

        }
    }
}
