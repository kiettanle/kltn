﻿namespace ThreadComposer
{
    partial class Audio
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Audio));
            this.axAudio = new AxWMPLib.AxWindowsMediaPlayer();
            this.timerMediaPlayer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.axAudio)).BeginInit();
            this.SuspendLayout();
            // 
            // axAudio
            // 
            this.axAudio.Enabled = true;
            this.axAudio.Location = new System.Drawing.Point(3, 3);
            this.axAudio.Name = "axAudio";
            this.axAudio.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axAudio.OcxState")));
            this.axAudio.Size = new System.Drawing.Size(535, 42);
            this.axAudio.TabIndex = 0;
            // 
            // Audio
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 48);
            this.Controls.Add(this.axAudio);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Audio";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Audio_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.axAudio)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AxWMPLib.AxWindowsMediaPlayer axAudio;
        private System.Windows.Forms.Timer timerMediaPlayer;

    }
}