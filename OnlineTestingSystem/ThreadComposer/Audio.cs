﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThreadComposer
{
    public partial class Audio : Form
    {
        public Audio()
        {
            InitializeComponent();
        }
      
        //đặt URL file cho media player
        public void ShowAudio(string url)
        {
            axAudio.URL = url;
        }

        private void Audio_FormClosed(object sender, FormClosedEventArgs e)
        {
            //dừng media player khi đóng form
            axAudio.Ctlcontrols.stop();
        }

        private void Audio_FormClosing(object sender, FormClosingEventArgs e)
        {
            axAudio.close();
        }
    }
}
