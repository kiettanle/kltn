﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DevExpress.XtraBars;
using DevExpress.XtraBars.Ribbon;
using DevExpress.XtraBars.Helpers;
using DevExpress.Skins;
using DevExpress.LookAndFeel;
using DevExpress.UserSkins;
using System.Windows;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Xml.Linq;
using System.Xml;
using Newtonsoft.Json;
namespace ThreadComposer
{
    public partial class FormMain : RibbonForm
    {
        public FormMain()
        {
            InitializeComponent();
            InitSkinGallery();
        }
        public static string FileName = "";
        public static string FileNameOpen = "";
        public static string Password = "";
        public static string ThreadPassword = "";

        public static XmlDocument xdoc;
        public static XmlNode root;

        public static List<Question> thread;
        public static int currentQuestion;
        public static string tabName;
        public static bool lvQuestionClick = false;
        public static bool newClick = false;

        void InitSkinGallery()
        {
            SkinHelper.InitSkinGallery(rgbiSkins, true);
        }

        private void iHelp_ItemClick(object sender, ItemClickEventArgs e)
        {
            MessageBox.Show("Hỗ trợ từ trái tim", "Mô tả", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void iAbout_ItemClick(object sender, ItemClickEventArgs e)
        {
            MessageBox.Show("Tác giả", "Mô tả\nMô tả\n", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        private void barButtonItemNew_ItemClick(object sender, ItemClickEventArgs e)
        {
            //string path = Application.StartupPath.Substring(0, Application.StartupPath.Length - 10);
            //string filename = Path.GetFileName(txtAudio.Text);
            //File.Copy(txtAudio.Text, path + "\\Audios\\" + filename);
            //axAudio.URL = path + "\\Audios\\" + filename;
            //MessageBox.Show("Đã xong");

            lvQuestionClick = false;
            tabName = "Add";
            newClick = true;
            bool key = false;
            //Nếu chưa ghi file, chưa có file để ghi
            if (key == false)
            {
                //Tạo 1 file xml mới
                xdoc = new XmlDocument();

                //tạo declaration
                xdoc.CreateXmlDeclaration("1.0", "utf-8", null);
                root = xdoc.CreateNode(XmlNodeType.XmlDeclaration, "", "");

                //tạo node Thread
                XmlElement rootElement = xdoc.CreateElement("Thread");

                //thêm node đến document
                xdoc.AppendChild(rootElement);


                //bật biến key lên thành true
                key = true;
                //Mở hộp thoại lưu file
                //SaveFileDialog sfd = new SaveFileDialog();
                ////Set định dạng file
                //sfd.Filter = "XML file(*.xml)|*.xml";
                //sfd.RestoreDirectory = true;
                //if (sfd.ShowDialog() == DialogResult.OK)
                //{
                FileName = Application.StartupPath.Substring(0, Application.StartupPath.Length - 10) + ".xml";
                if (FileName == "")
                {
                    return;
                }
                //Lưu file lại
                xdoc.Save(FileName);
                //Bật các nút
                barButtonItemExport.Enabled = true;
                barButtonItemNew.Enabled = false;
                barButtonItemTrueFalse.Enabled = true;
                barButtonItemFilling.Enabled = true;
                barButtonItemMatch.Enabled = true;
                barButtonItemMultiChoice.Enabled = true;
                barButtonItemSingleChoice.Enabled = true;
            }
        }
        private void barButtonItemImport_ItemClick(object sender, ItemClickEventArgs e)
        {
            lvQuestionClick = false;
            tabName = "View";
            newClick = false;
            // Create an instance of the open file dialog box.
            OpenFileDialog openFileDialog = new OpenFileDialog();
            // Set filter options and filter index.
            openFileDialog.Filter = "Chọn file (.xml)|*.xml|All Files (*.*)|*.*";
            openFileDialog.FilterIndex = 1;
            // Process input if the user clicked OK.
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                // Open the selected file to read.
                Stream fileStream = openFileDialog.OpenFile();
                using (StreamReader reader = new System.IO.StreamReader(fileStream))
                {
                    FileNameOpen = openFileDialog.FileName;
                    ReadXmlFile(FileNameOpen);
                }
                fileStream.Close();

                for (int i = 0; i < thread.Count(); i++)
                {
                    lvListQuestion.Items.Add((i + 1).ToString()).SubItems.Add(thread[i].Content);
                }
            }
            barButtonItemExport.Enabled = true;
        }
        //cập nhật ListQuestion
        public void ListQuestionUpdate(List<Question> ques)
        {
            lvListQuestion.Items.Clear();
            for (int i = 0; i < ques.Count(); i++)
            {
                lvListQuestion.Items.Add((i + 1).ToString()).SubItems.Add(ques[i].Content);
            }
        }
        //Đọc đề thi từ file xml
        public void ReadXmlFile(string f)
        {
            lvQuestionClick = false;
            lvListQuestion.Items.Clear();
            if (f.EndsWith(".xml"))
            {
                XmlDocument xd = new XmlDocument();
                xd.Load(f);

                StringWriter sw = new StringWriter();
                XmlTextWriter xw = new XmlTextWriter(sw);
                xd.WriteTo(xw);

                string xmlString = sw.ToString();

                var doc = XDocument.Parse(xmlString);
                thread = (from r in doc.Root.Elements("Question")
                          select new Question()
                          {
                              _Type = r.Element("Type").Value.ToString(),
                              Content = r.Element("Content").Value.ToString(),
                              Choices = (from c in r.Elements("Choices").Elements("Choice")
                                         select new Choice()
                                         {
                                             _Choice = c.Value.ToString()
                                         }).ToList(),
                              Choices1 = (from c in r.Elements("Choices1").Elements("Choice")
                                          select new Choice()
                                          {
                                              _Choice = c.Value.ToString()
                                          }).ToList(),
                              Answer = r.Element("Answer").Value.ToString(),
                              Picture = r.Element("Picture").Value.ToString(),
                              Audio = r.Element("Audio").Value.ToString(),
                              Video = r.Element("Video").Value.ToString()
                          }).ToList();
            }
            else
            {
                return;
            }
        }
        public void TabCreating(DevExpress.XtraTab.XtraTabControl TabControl, string Text, System.Windows.Forms.UserControl Form)
        {
            int Index = CheckExistTab(TabControl, Text);
            if (Index >= 0)
            {
                TabControl.SelectedTabPage = TabControl.TabPages[Index];
                TabControl.SelectedTabPage.Text = Text;
            }
            else
            {
                // DevExpress.XtraTab.XtraTabControl TabPage = new DevExpress.XtraTab.XtraTabControl();
                DevExpress.XtraTab.XtraTabPage TabPage = new DevExpress.XtraTab.XtraTabPage { Text = Text };
                TabControl.TabPages.Add(TabPage);
                TabControl.SelectedTabPage = TabPage;
                //Form.TopLevel = false;
                Form.Parent = TabPage;
                Form.Show();
                Form.Dock = DockStyle.Fill;
            }
        }
        public static int CheckExistTab(DevExpress.XtraTab.XtraTabControl TabControlName, string TabName)
        {
            int temp = -1;
            for (int i = 0; i < TabControlName.TabPages.Count; i++)
            {
                if (TabControlName.TabPages[i].Text == TabName)
                {
                    temp = i;
                    break;
                }
            }
            return temp;
        }
        public bool CheckPassword(string password)
        {
            bool flag = false;
            return flag;
        }
        private void FormMain_Load(object sender, EventArgs e)
        {

        }

        private void xtraTabControl1_CloseButtonClick(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage.Dispose();
        }

        //Đóng Tab hiện tại khi Click x
        private void xtraTab_CloseButtonClick(object sender, EventArgs e)
        {
            xtraTabControl1.SelectedTabPage.Dispose();
        }

        //Mở Tab True False
        private void barButtonItemTrueFalse_ItemClick(object sender, ItemClickEventArgs e)
        {
            lvQuestionClick = false;
            int t = 0;
            foreach (DevExpress.XtraTab.XtraTabPage tab in xtraTabControl1.TabPages)
            {
                if (tab.Text == "Câu hỏi đúng sai")
                {
                    xtraTabControl1.SelectedTabPage = tab;
                    t = 1;
                }
            }
            if (t == 1)
            {

            }
            else
            {
                TabCreating(xtraTabControl1, "Câu hỏi đúng sai", new ucTrueFalse(this));
            }
        }
        //Mở Tab Single Choice
        private void barButtonItemSingleChoice_ItemClick(object sender, ItemClickEventArgs e)
        {
            lvQuestionClick = false;
            int t = 0;
            foreach (DevExpress.XtraTab.XtraTabPage tab in xtraTabControl1.TabPages)
            {
                if (tab.Text == "Câu hỏi một lựa chọn")
                {
                    xtraTabControl1.SelectedTabPage = tab;
                    t = 1;
                }
            }
            if (t == 1)
            {

            }
            else
            {
                TabCreating(xtraTabControl1, "Câu hỏi một lựa chọn", new ucSingleChoice(this));
            }
        }
        //Mở Tab Câu hỏi nhiều lựa chọn
        private void barButtonItemMultiChoice_ItemClick(object sender, ItemClickEventArgs e)
        {
            lvQuestionClick = false;
            int t = 0;

            foreach (DevExpress.XtraTab.XtraTabPage tab in xtraTabControl1.TabPages)
            {
                if (tab.Text == "Câu hỏi nhiều lựa chọn")
                {
                    xtraTabControl1.SelectedTabPage = tab;
                    t = 1;
                }
            }
            if (t == 1)
            {

            }
            else
            {
                TabCreating(xtraTabControl1, "Câu hỏi nhiều lựa chọn", new ucMultiChoice(this));
            }
        }
        //Mở tab Câu hỏi điền khuyết
        private void barButtonItemFilling_ItemClick(object sender, ItemClickEventArgs e)
        {
            lvQuestionClick = false;
            int t = 0;

            foreach (DevExpress.XtraTab.XtraTabPage tab in xtraTabControl1.TabPages)
            {
                if (tab.Text == "Câu hỏi điền khuyết")
                {
                    xtraTabControl1.SelectedTabPage = tab;
                    t = 1;
                }
            }
            if (t == 1)
            {

            }
            else
            {
                TabCreating(xtraTabControl1, "Câu hỏi điền khuyết", new ucFilling());
            }
        }
        //Mở Tab câu hỏi ghép hợp
        private void barButtonItemMatch_ItemClick(object sender, ItemClickEventArgs e)
        {
            lvQuestionClick = false;
            int t = 0;
            foreach (DevExpress.XtraTab.XtraTabPage tab in xtraTabControl1.TabPages)
            {
                if (tab.Text == "Câu hỏi ghép hợp")
                {
                    xtraTabControl1.SelectedTabPage = tab;
                    t = 1;
                }
            }
            if (t == 1)
            {

            }
            else
            {
                TabCreating(xtraTabControl1, "Câu hỏi ghép hợp", new ucMatching(this));
            }
        }
        //Xuất đề thi ra file định dạng *.ots
        private void barButtonItemExport_ItemClick(object sender, ItemClickEventArgs e)
        {
            //OpenFileDialog openFileDialog = new OpenFileDialog();
            //// Set filter options and filter index.
            //openFileDialog.Filter = "Chọn file (.xml)|*.xml|All Files (*.*)|*.*";
            //openFileDialog.ShowDialog();
            string xml = XMLToString(FileName);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);
            string jsonText = JsonConvert.SerializeXmlNode(doc);
            jsonText = jsonText.Replace("\\", "").Replace(@"\", "").Replace("\n", "").Replace("\r", "").Replace("\t", "");
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Filter = "Lưu dưới dạng (.ots)|*.ots";
            sfd.ShowDialog();
            File.WriteAllText(sfd.FileName, jsonText);
            MessageBox.Show("Lưu thành công", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        //Hàm chuyển nội dung trong file Xml sang string
        public string XMLToString(string f)
        {
            XmlDocument xd = new XmlDocument();
            xd.Load(f);

            StringWriter sw = new StringWriter();
            XmlTextWriter xw = new XmlTextWriter(sw);
            xd.WriteTo(xw);

            string xmlString = sw.ToString();
            xmlString = xmlString.Replace("\\", "").Replace(@"\", "").Replace("\n", "").Replace("\r", "").Replace("\t", "");
            return xmlString;
        }
        //Bắt sự kiện Click vào danh sách câu hỏi
        private void lvListQuestion_Click(object sender, EventArgs e)
        {
            lvQuestionClick = true;
            DevExpress.XtraTab.XtraTabPage tabPage = new DevExpress.XtraTab.XtraTabPage();
            int t = 0;
            foreach (DevExpress.XtraTab.XtraTabPage tab in xtraTabControl1.TabPages)
            {

                if (tab.Text == "View")
                {
                    xtraTabControl1.SelectedTabPage = tab;
                    tabPage = tab;
                    t = 1;

                }
            }
            if (t == 1)
            {
                xtraTabControl1.TabPages.Remove(tabPage);

            }

            currentQuestion = lvListQuestion.Items.IndexOf(lvListQuestion.SelectedItems[0]);
            if (newClick)
                tabName = "Add";
            else
                tabName = "View";
            if (thread[currentQuestion]._Type == "MultiChoice")
            {
                TabCreating(xtraTabControl1, "View", new ucMultiChoice(this));
            }
            else
                if (thread[currentQuestion]._Type == "SingleChoice")
                {
                    TabCreating(xtraTabControl1, "View", new ucSingleChoice(this));
                    //   lvQuestionList = ListViewQuestion;
                }
                else
                    if (thread[currentQuestion]._Type == "Filling")
                    {
                        TabCreating(xtraTabControl1, "View", new ucFilling());
                        // lvQuestionList = ListViewQuestion;
                    }
                    else
                        if (thread[currentQuestion]._Type == "Matching")
                        {
                            TabCreating(xtraTabControl1, "View", new ucMatching(this));
                            // lvQuestionList = ListViewQuestion;
                        }
                        else
                            if (thread[currentQuestion]._Type == "TrueFalse")
                            {
                                TabCreating(xtraTabControl1, "View", new ucTrueFalse(this));
                                //lvQuestionList = ListViewQuestion;
                            }
        }
    }
}