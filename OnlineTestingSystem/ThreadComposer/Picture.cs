﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThreadComposer
{
    public partial class Picture : Form
    {
        public Picture()
        {
            InitializeComponent();
        }
        public void ShowPicture(string url)
        {
            //InitializeComponent();

            Picture media = new Picture();
            PictureBox picture = new PictureBox();
            Image image = UserPicture.Base64ToImage(UserPicture.ImageToBase64(url));
            picture.Width = image.Width;
            picture.Height = image.Height;
            picture.Image = image;
            media.Text = url;
            media.Controls.Add(picture);
            media.Width = image.Width;
            media.Height = image.Height;
            media.ShowDialog();
        //    btnDeletePicture.Enabled = true;
        }
    }
}
