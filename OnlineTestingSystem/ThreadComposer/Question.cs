﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreadComposer
{
    public class Question
    {
        public string _Type { get; set; }
        public string Content { get; set; }
        public List<Choice> Choices { get; set; }
        public List<Choice> Choices1 { get; set; }
        public string Answer { get; set; }
        public string Picture { get; set; }
        public string Audio { get; set; }
        public string Video { get; set; }
        
        public Question()
        {

        }
    }
}
