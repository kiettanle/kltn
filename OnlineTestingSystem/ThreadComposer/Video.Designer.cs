﻿namespace ThreadComposer
{
    partial class Video
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Video));
            this.axVideo = new AxWMPLib.AxWindowsMediaPlayer();
            ((System.ComponentModel.ISupportInitialize)(this.axVideo)).BeginInit();
            this.SuspendLayout();
            // 
            // axVideo
            // 
            this.axVideo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.axVideo.Enabled = true;
            this.axVideo.Location = new System.Drawing.Point(0, 0);
            this.axVideo.Name = "axVideo";
            this.axVideo.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axVideo.OcxState")));
            this.axVideo.Size = new System.Drawing.Size(675, 399);
            this.axVideo.TabIndex = 0;
            // 
            // Video
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 399);
            this.Controls.Add(this.axVideo);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Video";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Video";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Video_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.axVideo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private AxWMPLib.AxWindowsMediaPlayer axVideo;
    }
}