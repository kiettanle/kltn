﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ThreadComposer
{
    public partial class Video : Form
    {
        public Video()
        {
            InitializeComponent();
        }

        public void ShowVideo(string url)
        {
            axVideo.URL = url;
        }

        private void Video_FormClosed(object sender, FormClosedEventArgs e)
        {
            axVideo.Ctlcontrols.stop();
        }

        private void Video_FormClosing(object sender, FormClosingEventArgs e)
        {
            axVideo.close();
        }
    }
}
