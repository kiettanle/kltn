﻿namespace ThreadComposer
{
    partial class ucFilling
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucFilling));
            this.groupControlSinggleChoice = new DevExpress.XtraEditors.GroupControl();
            this.btnUpdate = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteAudio = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeletePicture = new DevExpress.XtraEditors.SimpleButton();
            this.btnViewVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btnViewAudio = new DevExpress.XtraEditors.SimpleButton();
            this.btnViewPicture = new DevExpress.XtraEditors.SimpleButton();
            this.txtAudio = new System.Windows.Forms.TextBox();
            this.txtVideo = new System.Windows.Forms.TextBox();
            this.txtPicture = new System.Windows.Forms.TextBox();
            this.btnChooseVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btnChooseAudio = new DevExpress.XtraEditors.SimpleButton();
            this.btnChoosePicture = new DevExpress.XtraEditors.SimpleButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtAnswer = new System.Windows.Forms.RichTextBox();
            this.txtContent = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlSinggleChoice)).BeginInit();
            this.groupControlSinggleChoice.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControlSinggleChoice
            // 
            this.groupControlSinggleChoice.Controls.Add(this.btnUpdate);
            this.groupControlSinggleChoice.Controls.Add(this.btnDelete);
            this.groupControlSinggleChoice.Controls.Add(this.btnSave);
            this.groupControlSinggleChoice.Controls.Add(this.btnDeleteVideo);
            this.groupControlSinggleChoice.Controls.Add(this.btnDeleteAudio);
            this.groupControlSinggleChoice.Controls.Add(this.btnDeletePicture);
            this.groupControlSinggleChoice.Controls.Add(this.btnViewVideo);
            this.groupControlSinggleChoice.Controls.Add(this.btnViewAudio);
            this.groupControlSinggleChoice.Controls.Add(this.btnViewPicture);
            this.groupControlSinggleChoice.Controls.Add(this.txtAudio);
            this.groupControlSinggleChoice.Controls.Add(this.txtVideo);
            this.groupControlSinggleChoice.Controls.Add(this.txtPicture);
            this.groupControlSinggleChoice.Controls.Add(this.btnChooseVideo);
            this.groupControlSinggleChoice.Controls.Add(this.btnChooseAudio);
            this.groupControlSinggleChoice.Controls.Add(this.btnChoosePicture);
            this.groupControlSinggleChoice.Controls.Add(this.label5);
            this.groupControlSinggleChoice.Controls.Add(this.label8);
            this.groupControlSinggleChoice.Controls.Add(this.txtAnswer);
            this.groupControlSinggleChoice.Controls.Add(this.txtContent);
            this.groupControlSinggleChoice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupControlSinggleChoice.Location = new System.Drawing.Point(0, 0);
            this.groupControlSinggleChoice.Name = "groupControlSinggleChoice";
            this.groupControlSinggleChoice.Size = new System.Drawing.Size(965, 245);
            this.groupControlSinggleChoice.TabIndex = 22;
            this.groupControlSinggleChoice.Text = "Câu hỏi điền khuyết";
            // 
            // btnUpdate
            // 
            this.btnUpdate.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnUpdate.Image = ((System.Drawing.Image)(resources.GetObject("btnUpdate.Image")));
            this.btnUpdate.Location = new System.Drawing.Point(459, 212);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(75, 23);
            this.btnUpdate.TabIndex = 58;
            this.btnUpdate.Text = "Cập nhật";
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(540, 212);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 57;
            this.btnDelete.Text = "Xóa";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(378, 212);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 56;
            this.btnSave.Text = "Lưu";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDeleteVideo
            // 
            this.btnDeleteVideo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteVideo.Enabled = false;
            this.btnDeleteVideo.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteVideo.Image")));
            this.btnDeleteVideo.Location = new System.Drawing.Point(924, 183);
            this.btnDeleteVideo.Name = "btnDeleteVideo";
            this.btnDeleteVideo.Size = new System.Drawing.Size(23, 23);
            this.btnDeleteVideo.TabIndex = 55;
            this.btnDeleteVideo.Click += new System.EventHandler(this.btnDeleteVideo_Click);
            // 
            // btnDeleteAudio
            // 
            this.btnDeleteAudio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteAudio.Enabled = false;
            this.btnDeleteAudio.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteAudio.Image")));
            this.btnDeleteAudio.Location = new System.Drawing.Point(924, 154);
            this.btnDeleteAudio.Name = "btnDeleteAudio";
            this.btnDeleteAudio.Size = new System.Drawing.Size(23, 23);
            this.btnDeleteAudio.TabIndex = 54;
            this.btnDeleteAudio.Click += new System.EventHandler(this.btnDeleteAudio_Click);
            // 
            // btnDeletePicture
            // 
            this.btnDeletePicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeletePicture.Enabled = false;
            this.btnDeletePicture.Image = ((System.Drawing.Image)(resources.GetObject("btnDeletePicture.Image")));
            this.btnDeletePicture.Location = new System.Drawing.Point(924, 125);
            this.btnDeletePicture.Name = "btnDeletePicture";
            this.btnDeletePicture.Size = new System.Drawing.Size(23, 23);
            this.btnDeletePicture.TabIndex = 53;
            this.btnDeletePicture.Click += new System.EventHandler(this.btnDeletePicture_Click);
            // 
            // btnViewVideo
            // 
            this.btnViewVideo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewVideo.Enabled = false;
            this.btnViewVideo.Image = ((System.Drawing.Image)(resources.GetObject("btnViewVideo.Image")));
            this.btnViewVideo.Location = new System.Drawing.Point(895, 183);
            this.btnViewVideo.Name = "btnViewVideo";
            this.btnViewVideo.Size = new System.Drawing.Size(23, 23);
            this.btnViewVideo.TabIndex = 52;
            this.btnViewVideo.Click += new System.EventHandler(this.btnViewVideo_Click);
            // 
            // btnViewAudio
            // 
            this.btnViewAudio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewAudio.Enabled = false;
            this.btnViewAudio.Image = ((System.Drawing.Image)(resources.GetObject("btnViewAudio.Image")));
            this.btnViewAudio.Location = new System.Drawing.Point(895, 154);
            this.btnViewAudio.Name = "btnViewAudio";
            this.btnViewAudio.Size = new System.Drawing.Size(23, 23);
            this.btnViewAudio.TabIndex = 51;
            this.btnViewAudio.Click += new System.EventHandler(this.btnViewAudio_Click);
            // 
            // btnViewPicture
            // 
            this.btnViewPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewPicture.Enabled = false;
            this.btnViewPicture.Image = ((System.Drawing.Image)(resources.GetObject("btnViewPicture.Image")));
            this.btnViewPicture.Location = new System.Drawing.Point(895, 125);
            this.btnViewPicture.Name = "btnViewPicture";
            this.btnViewPicture.Size = new System.Drawing.Size(23, 23);
            this.btnViewPicture.TabIndex = 50;
            this.btnViewPicture.Click += new System.EventHandler(this.btnViewPicture_Click);
            // 
            // txtAudio
            // 
            this.txtAudio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAudio.Location = new System.Drawing.Point(121, 156);
            this.txtAudio.Name = "txtAudio";
            this.txtAudio.Size = new System.Drawing.Size(768, 21);
            this.txtAudio.TabIndex = 49;
            // 
            // txtVideo
            // 
            this.txtVideo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVideo.Location = new System.Drawing.Point(121, 185);
            this.txtVideo.Name = "txtVideo";
            this.txtVideo.Size = new System.Drawing.Size(768, 21);
            this.txtVideo.TabIndex = 48;
            // 
            // txtPicture
            // 
            this.txtPicture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPicture.Location = new System.Drawing.Point(121, 125);
            this.txtPicture.Name = "txtPicture";
            this.txtPicture.Size = new System.Drawing.Size(768, 21);
            this.txtPicture.TabIndex = 47;
            // 
            // btnChooseVideo
            // 
            this.btnChooseVideo.Image = ((System.Drawing.Image)(resources.GetObject("btnChooseVideo.Image")));
            this.btnChooseVideo.Location = new System.Drawing.Point(32, 183);
            this.btnChooseVideo.Name = "btnChooseVideo";
            this.btnChooseVideo.Size = new System.Drawing.Size(75, 23);
            this.btnChooseVideo.TabIndex = 46;
            this.btnChooseVideo.Text = "Video";
            this.btnChooseVideo.Click += new System.EventHandler(this.btnChooseVideo_Click);
            // 
            // btnChooseAudio
            // 
            this.btnChooseAudio.Image = ((System.Drawing.Image)(resources.GetObject("btnChooseAudio.Image")));
            this.btnChooseAudio.Location = new System.Drawing.Point(32, 154);
            this.btnChooseAudio.Name = "btnChooseAudio";
            this.btnChooseAudio.Size = new System.Drawing.Size(75, 23);
            this.btnChooseAudio.TabIndex = 45;
            this.btnChooseAudio.Text = "Âm thanh";
            this.btnChooseAudio.Click += new System.EventHandler(this.btnChooseAudio_Click);
            // 
            // btnChoosePicture
            // 
            this.btnChoosePicture.Image = ((System.Drawing.Image)(resources.GetObject("btnChoosePicture.Image")));
            this.btnChoosePicture.Location = new System.Drawing.Point(32, 125);
            this.btnChoosePicture.Name = "btnChoosePicture";
            this.btnChoosePicture.Size = new System.Drawing.Size(75, 23);
            this.btnChoosePicture.TabIndex = 44;
            this.btnChoosePicture.Text = "Hình ảnh";
            this.btnChoosePicture.Click += new System.EventHandler(this.btnChoosePicture_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(14, 93);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Phương án đúng :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(14, 46);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Nội dung câu hỏi :";
            // 
            // txtAnswer
            // 
            this.txtAnswer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAnswer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAnswer.Location = new System.Drawing.Point(121, 79);
            this.txtAnswer.Multiline = false;
            this.txtAnswer.Name = "txtAnswer";
            this.txtAnswer.Size = new System.Drawing.Size(824, 40);
            this.txtAnswer.TabIndex = 8;
            this.txtAnswer.Text = "";
            // 
            // txtContent
            // 
            this.txtContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtContent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtContent.Location = new System.Drawing.Point(121, 31);
            this.txtContent.Name = "txtContent";
            this.txtContent.Size = new System.Drawing.Size(824, 40);
            this.txtContent.TabIndex = 7;
            this.txtContent.Text = "";
            // 
            // ucFilling
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControlSinggleChoice);
            this.Name = "ucFilling";
            this.Size = new System.Drawing.Size(965, 245);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlSinggleChoice)).EndInit();
            this.groupControlSinggleChoice.ResumeLayout(false);
            this.groupControlSinggleChoice.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControlSinggleChoice;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RichTextBox txtAnswer;
        private System.Windows.Forms.RichTextBox txtContent;
        private DevExpress.XtraEditors.SimpleButton btnUpdate;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnDeleteVideo;
        private DevExpress.XtraEditors.SimpleButton btnDeleteAudio;
        private DevExpress.XtraEditors.SimpleButton btnDeletePicture;
        private DevExpress.XtraEditors.SimpleButton btnViewVideo;
        private DevExpress.XtraEditors.SimpleButton btnViewAudio;
        private DevExpress.XtraEditors.SimpleButton btnViewPicture;
        private System.Windows.Forms.TextBox txtAudio;
        private System.Windows.Forms.TextBox txtVideo;
        private System.Windows.Forms.TextBox txtPicture;
        private DevExpress.XtraEditors.SimpleButton btnChooseVideo;
        private DevExpress.XtraEditors.SimpleButton btnChooseAudio;
        private DevExpress.XtraEditors.SimpleButton btnChoosePicture;
    }
}
