﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml;

namespace ThreadComposer
{
    public partial class ucFilling : UserControl
    {
        public ucFilling()
        {
            InitializeComponent();
            txtContent.Focus();
        }
        public void Reset()
        {
            txtContent.ResetText();
            txtAnswer.ResetText();
            PictureURL = "";
            AudioURL = "";
            VideoURL = "";
            btnViewAudio.Enabled = false;
            btnViewPicture.Enabled = false;
            btnViewVideo.Enabled = false;
            btnDeleteAudio.Enabled = false;
            btnDeletePicture.Enabled = false;
            btnDeleteVideo.Enabled = false;
            txtAudio.ResetText();
            txtPicture.ResetText();
            txtVideo.ResetText();
            txtContent.Focus();
        }

        //Lấy tên file từ FormMain
        public string FileName = FormMain.FileName;
        //Các biến lưu đường dẫn của media
        public static string PictureURL = "";
        public static string AudioURL = "";
        public static string VideoURL = "";

        //Hàm ghi câu hỏi vào file
        private void Write()
        {
            FormMain.xdoc.Load(FileName);
            XmlElement question = FormMain.xdoc.CreateElement("Question");
            FormMain.xdoc.DocumentElement.PrependChild(question);
            FormMain.xdoc.ChildNodes.Item(0).AppendChild(question);

            // Create <installationid> Node
            XmlElement id = FormMain.xdoc.CreateElement("ID");
            XmlText idText = FormMain.xdoc.CreateTextNode(Guid.NewGuid().ToString());
            id.AppendChild(idText);
            question.PrependChild(id);

            // Create <installationid> Node
            XmlElement type = FormMain.xdoc.CreateElement("Type");
            XmlText typeText = FormMain.xdoc.CreateTextNode("Filling");
            type.AppendChild(typeText);
            question.InsertAfter(type, id);

            // Create <environment> Node
            XmlElement content = FormMain.xdoc.CreateElement("Content");
            XmlText contentText = FormMain.xdoc.CreateTextNode(txtContent.Text.ToString());
            content.AppendChild(contentText);
            question.InsertAfter(content, type);

            // Create <environment> Node
            XmlElement answer = FormMain.xdoc.CreateElement("Answer");
            XmlText answerText = FormMain.xdoc.CreateTextNode(txtAnswer.Text.ToString());
            answer.AppendChild(answerText);
            question.InsertAfter(answer, content);

            // Create <environment> Node
            XmlElement picture = FormMain.xdoc.CreateElement("Picture");
            XmlText pictureText = FormMain.xdoc.CreateTextNode(PictureURL.ToString().Equals("") ? "" : UserPicture.ImageToBase64(PictureURL));
            picture.AppendChild(pictureText);
            question.InsertAfter(picture, answer);

            // Create <environment> Node
            XmlElement audio = FormMain.xdoc.CreateElement("Audio");
            XmlText audioText = FormMain.xdoc.CreateTextNode(txtAudio.Text.ToString());
            audio.AppendChild(audioText);
            question.InsertAfter(audio, picture);

            // Create <environment> Node
            XmlElement video = FormMain.xdoc.CreateElement("Video");
            XmlText videoText = FormMain.xdoc.CreateTextNode(txtVideo.Text.ToString());
            video.AppendChild(videoText);
            question.InsertAfter(video, audio);

            FormMain.xdoc.Save(FileName);

            // Reset();
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            //Ghi thêm vào file
            Write();
            Reset();
        }

        //Chọn hình ảnh
        private void btnChoosePicture_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "Chọn hình ảnh (*.jpg)|*.jpg*";
                ofd.ShowDialog();
                string fileName = ofd.FileName;
                PictureURL = fileName;
                //PictureURL = Application.StartupPath.Substring(0, Application.StartupPath.Length - 10) + Path.GetFileName(openFileDialog.FileName);
                if (string.IsNullOrEmpty(fileName))
                    return;
                txtPicture.Text = fileName;
                //pictureBox.Image = Base64ToImage(ImageToBase64(PictureURL));
                //pictureBox.ImageLocation = fileName;
                btnChoosePicture.BackColor = Color.Red;
                btnViewPicture.Enabled = true;
                btnDeletePicture.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Vui lòng chọn file ảnh nhé !");
            }
        }

        //Chọn âm thanh
        private void btnChooseAudio_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Chọn âm thanh (*.mp3)|*.mp3*";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                txtAudio.Text = ofd.FileName;
                //axAudio.URL = ofd.FileName;
                AudioURL = ofd.FileName;
                btnViewAudio.Enabled = true;
                btnDeleteAudio.Enabled = true;
            }
        }

        //Chọn video
        private void btnChooseVideo_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Chọn video (*.mp4*)|*.mp4*";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                txtVideo.Text = ofd.FileName;
                VideoURL = ofd.FileName;
                btnViewVideo.Enabled = true;
                btnDeleteVideo.Enabled = true;
            }
        }

        //Xem hình
        private void btnViewPicture_Click(object sender, EventArgs e)
        {
            Picture media = new Picture();
            PictureBox picture = new PictureBox();
            Image image = UserPicture.Base64ToImage(UserPicture.ImageToBase64(txtPicture.Text));
            picture.Width = image.Width;
            picture.Height = image.Height;
            picture.Image = image;
            media.Text = txtPicture.Text;
            media.Controls.Add(picture);
            media.Width = image.Width;
            media.Height = image.Height;
            media.ShowDialog();
            btnDeletePicture.Enabled = true;
        }

        //Nghe âm thanh
        private void btnViewAudio_Click(object sender, EventArgs e)
        {
            Audio audio = new Audio();
            //WindowsMediaPlayer wplayer = new WindowsMediaPlayer();
            //wplayer.URL = AudioURL;
            //wplayer.controls.play();
            audio.ShowDialog();
            btnDeleteAudio.Enabled = true;
        }

        //Xem video
        private void btnViewVideo_Click(object sender, EventArgs e)
        {
            Video video = new Video();
            //WindowsMediaPlayer wplayer = new WindowsMediaPlayer();
            //wplayer.URL = AudioURL;
            //wplayer.controls.play();
            video.Text = txtVideo.Text;
            video.ShowDialog();
            btnDeleteVideo.Enabled = true;
        }

        //Xóa hình
        private void btnDeletePicture_Click(object sender, EventArgs e)
        {
            txtPicture.ResetText();
            btnViewPicture.Enabled = false;
            btnDeletePicture.Enabled = false;
        }

        //Xóa âm thanh
        private void btnDeleteAudio_Click(object sender, EventArgs e)
        {
            txtAudio.ResetText();
            btnViewAudio.Enabled = false;
            btnDeleteAudio.Enabled = false;
        }

        //Xóa video
        private void btnDeleteVideo_Click(object sender, EventArgs e)
        {
            txtVideo.ResetText();
            btnViewVideo.Enabled = false;
            btnDeleteVideo.Enabled = false;
        }
    }
}
