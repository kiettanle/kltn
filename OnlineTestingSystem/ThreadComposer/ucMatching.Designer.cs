﻿namespace ThreadComposer
{
    partial class ucMatching
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucMatching));
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtContent = new System.Windows.Forms.RichTextBox();
            this.txtChoice = new System.Windows.Forms.RichTextBox();
            this.btnDeleteVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteAudio = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeletePicture = new DevExpress.XtraEditors.SimpleButton();
            this.btnViewVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btnViewAudio = new DevExpress.XtraEditors.SimpleButton();
            this.colChoice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnAddChoice = new DevExpress.XtraEditors.SimpleButton();
            this.lvChoice = new System.Windows.Forms.ListView();
            this.btnViewPicture = new DevExpress.XtraEditors.SimpleButton();
            this.txtAudio = new System.Windows.Forms.TextBox();
            this.txtVideo = new System.Windows.Forms.TextBox();
            this.txtPicture = new System.Windows.Forms.TextBox();
            this.btnChooseVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btnChooseAudio = new DevExpress.XtraEditors.SimpleButton();
            this.btnChoosePicture = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteChoice = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditChoice = new DevExpress.XtraEditors.SimpleButton();
            this.groupControlSinggleChoice = new DevExpress.XtraEditors.GroupControl();
            this.txtAnswer = new System.Windows.Forms.RichTextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnDeleteChoice1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditChoice1 = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddChoice1 = new DevExpress.XtraEditors.SimpleButton();
            this.lvChoice1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.txtChoice1 = new System.Windows.Forms.RichTextBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlSinggleChoice)).BeginInit();
            this.groupControlSinggleChoice.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(427, 503);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 59;
            this.btnSave.Text = "Lưu";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(72, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Cột A :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(14, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Nội dung câu hỏi :";
            // 
            // txtContent
            // 
            this.txtContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtContent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtContent.Location = new System.Drawing.Point(121, 37);
            this.txtContent.Name = "txtContent";
            this.txtContent.Size = new System.Drawing.Size(915, 34);
            this.txtContent.TabIndex = 7;
            this.txtContent.Text = "";
            // 
            // txtChoice
            // 
            this.txtChoice.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtChoice.Location = new System.Drawing.Point(121, 87);
            this.txtChoice.Name = "txtChoice";
            this.txtChoice.Size = new System.Drawing.Size(391, 34);
            this.txtChoice.TabIndex = 9;
            this.txtChoice.Text = "";
            // 
            // btnDeleteVideo
            // 
            this.btnDeleteVideo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteVideo.Enabled = false;
            this.btnDeleteVideo.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteVideo.Image")));
            this.btnDeleteVideo.Location = new System.Drawing.Point(988, 78);
            this.btnDeleteVideo.Name = "btnDeleteVideo";
            this.btnDeleteVideo.Size = new System.Drawing.Size(23, 23);
            this.btnDeleteVideo.TabIndex = 28;
            this.btnDeleteVideo.Click += new System.EventHandler(this.btnDeleteVideo_Click);
            // 
            // btnDeleteAudio
            // 
            this.btnDeleteAudio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteAudio.Enabled = false;
            this.btnDeleteAudio.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteAudio.Image")));
            this.btnDeleteAudio.Location = new System.Drawing.Point(988, 49);
            this.btnDeleteAudio.Name = "btnDeleteAudio";
            this.btnDeleteAudio.Size = new System.Drawing.Size(23, 23);
            this.btnDeleteAudio.TabIndex = 27;
            this.btnDeleteAudio.Click += new System.EventHandler(this.btnDeleteAudio_Click);
            // 
            // btnDeletePicture
            // 
            this.btnDeletePicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeletePicture.Enabled = false;
            this.btnDeletePicture.Image = ((System.Drawing.Image)(resources.GetObject("btnDeletePicture.Image")));
            this.btnDeletePicture.Location = new System.Drawing.Point(988, 20);
            this.btnDeletePicture.Name = "btnDeletePicture";
            this.btnDeletePicture.Size = new System.Drawing.Size(23, 23);
            this.btnDeletePicture.TabIndex = 26;
            this.btnDeletePicture.Click += new System.EventHandler(this.btnDeletePicture_Click);
            // 
            // btnViewVideo
            // 
            this.btnViewVideo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewVideo.Enabled = false;
            this.btnViewVideo.Image = ((System.Drawing.Image)(resources.GetObject("btnViewVideo.Image")));
            this.btnViewVideo.Location = new System.Drawing.Point(959, 78);
            this.btnViewVideo.Name = "btnViewVideo";
            this.btnViewVideo.Size = new System.Drawing.Size(23, 23);
            this.btnViewVideo.TabIndex = 25;
            this.btnViewVideo.Click += new System.EventHandler(this.btnViewVideo_Click);
            // 
            // btnViewAudio
            // 
            this.btnViewAudio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewAudio.Enabled = false;
            this.btnViewAudio.Image = ((System.Drawing.Image)(resources.GetObject("btnViewAudio.Image")));
            this.btnViewAudio.Location = new System.Drawing.Point(959, 49);
            this.btnViewAudio.Name = "btnViewAudio";
            this.btnViewAudio.Size = new System.Drawing.Size(23, 23);
            this.btnViewAudio.TabIndex = 24;
            this.btnViewAudio.Click += new System.EventHandler(this.btnViewAudio_Click);
            // 
            // colChoice
            // 
            this.colChoice.Text = "Phương án cột A";
            this.colChoice.Width = 383;
            // 
            // btnAddChoice
            // 
            this.btnAddChoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddChoice.Image = ((System.Drawing.Image)(resources.GetObject("btnAddChoice.Image")));
            this.btnAddChoice.Location = new System.Drawing.Point(121, 127);
            this.btnAddChoice.Name = "btnAddChoice";
            this.btnAddChoice.Size = new System.Drawing.Size(75, 23);
            this.btnAddChoice.TabIndex = 63;
            this.btnAddChoice.Text = "Thêm";
            this.btnAddChoice.Click += new System.EventHandler(this.btnAddChoice_Click);
            // 
            // lvChoice
            // 
            this.lvChoice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvChoice.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colChoice});
            this.lvChoice.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lvChoice.FullRowSelect = true;
            this.lvChoice.GridLines = true;
            this.lvChoice.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvChoice.Location = new System.Drawing.Point(120, 156);
            this.lvChoice.Name = "lvChoice";
            this.lvChoice.Size = new System.Drawing.Size(392, 188);
            this.lvChoice.TabIndex = 62;
            this.lvChoice.UseCompatibleStateImageBehavior = false;
            this.lvChoice.View = System.Windows.Forms.View.Details;
            this.lvChoice.Click += new System.EventHandler(this.lvChoice_Click);
            // 
            // btnViewPicture
            // 
            this.btnViewPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewPicture.Enabled = false;
            this.btnViewPicture.Image = ((System.Drawing.Image)(resources.GetObject("btnViewPicture.Image")));
            this.btnViewPicture.Location = new System.Drawing.Point(959, 20);
            this.btnViewPicture.Name = "btnViewPicture";
            this.btnViewPicture.Size = new System.Drawing.Size(23, 23);
            this.btnViewPicture.TabIndex = 23;
            this.btnViewPicture.Click += new System.EventHandler(this.btnViewPicture_Click);
            // 
            // txtAudio
            // 
            this.txtAudio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAudio.Location = new System.Drawing.Point(94, 51);
            this.txtAudio.Name = "txtAudio";
            this.txtAudio.Size = new System.Drawing.Size(859, 21);
            this.txtAudio.TabIndex = 22;
            // 
            // txtVideo
            // 
            this.txtVideo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVideo.Location = new System.Drawing.Point(94, 80);
            this.txtVideo.Name = "txtVideo";
            this.txtVideo.Size = new System.Drawing.Size(859, 21);
            this.txtVideo.TabIndex = 21;
            // 
            // txtPicture
            // 
            this.txtPicture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPicture.Location = new System.Drawing.Point(94, 20);
            this.txtPicture.Name = "txtPicture";
            this.txtPicture.Size = new System.Drawing.Size(859, 21);
            this.txtPicture.TabIndex = 19;
            // 
            // btnChooseVideo
            // 
            this.btnChooseVideo.Image = ((System.Drawing.Image)(resources.GetObject("btnChooseVideo.Image")));
            this.btnChooseVideo.Location = new System.Drawing.Point(7, 78);
            this.btnChooseVideo.Name = "btnChooseVideo";
            this.btnChooseVideo.Size = new System.Drawing.Size(75, 23);
            this.btnChooseVideo.TabIndex = 2;
            this.btnChooseVideo.Text = "Video";
            this.btnChooseVideo.Click += new System.EventHandler(this.btnChooseVideo_Click);
            // 
            // btnChooseAudio
            // 
            this.btnChooseAudio.Image = ((System.Drawing.Image)(resources.GetObject("btnChooseAudio.Image")));
            this.btnChooseAudio.Location = new System.Drawing.Point(7, 49);
            this.btnChooseAudio.Name = "btnChooseAudio";
            this.btnChooseAudio.Size = new System.Drawing.Size(75, 23);
            this.btnChooseAudio.TabIndex = 1;
            this.btnChooseAudio.Text = "Âm thanh";
            this.btnChooseAudio.Click += new System.EventHandler(this.btnChooseAudio_Click);
            // 
            // btnChoosePicture
            // 
            this.btnChoosePicture.Image = ((System.Drawing.Image)(resources.GetObject("btnChoosePicture.Image")));
            this.btnChoosePicture.Location = new System.Drawing.Point(7, 20);
            this.btnChoosePicture.Name = "btnChoosePicture";
            this.btnChoosePicture.Size = new System.Drawing.Size(75, 23);
            this.btnChoosePicture.TabIndex = 0;
            this.btnChoosePicture.Text = "Hình ảnh";
            this.btnChoosePicture.Click += new System.EventHandler(this.btnChoosePicture_Click);
            // 
            // btnDeleteChoice
            // 
            this.btnDeleteChoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteChoice.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteChoice.Image")));
            this.btnDeleteChoice.Location = new System.Drawing.Point(283, 127);
            this.btnDeleteChoice.Name = "btnDeleteChoice";
            this.btnDeleteChoice.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteChoice.TabIndex = 65;
            this.btnDeleteChoice.Text = "Xóa";
            this.btnDeleteChoice.Click += new System.EventHandler(this.btnDeleteChoice_Click);
            // 
            // btnEditChoice
            // 
            this.btnEditChoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditChoice.Image = ((System.Drawing.Image)(resources.GetObject("btnEditChoice.Image")));
            this.btnEditChoice.Location = new System.Drawing.Point(202, 127);
            this.btnEditChoice.Name = "btnEditChoice";
            this.btnEditChoice.Size = new System.Drawing.Size(75, 23);
            this.btnEditChoice.TabIndex = 64;
            this.btnEditChoice.Text = "Sửa";
            this.btnEditChoice.Click += new System.EventHandler(this.btnEditChoice_Click);
            // 
            // groupControlSinggleChoice
            // 
            this.groupControlSinggleChoice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControlSinggleChoice.Controls.Add(this.txtAnswer);
            this.groupControlSinggleChoice.Controls.Add(this.label2);
            this.groupControlSinggleChoice.Controls.Add(this.btnDeleteChoice1);
            this.groupControlSinggleChoice.Controls.Add(this.btnEditChoice1);
            this.groupControlSinggleChoice.Controls.Add(this.btnAddChoice1);
            this.groupControlSinggleChoice.Controls.Add(this.lvChoice1);
            this.groupControlSinggleChoice.Controls.Add(this.label1);
            this.groupControlSinggleChoice.Controls.Add(this.txtChoice1);
            this.groupControlSinggleChoice.Controls.Add(this.btnDeleteChoice);
            this.groupControlSinggleChoice.Controls.Add(this.btnEditChoice);
            this.groupControlSinggleChoice.Controls.Add(this.btnAddChoice);
            this.groupControlSinggleChoice.Controls.Add(this.lvChoice);
            this.groupControlSinggleChoice.Controls.Add(this.groupBox2);
            this.groupControlSinggleChoice.Controls.Add(this.btnDelete);
            this.groupControlSinggleChoice.Controls.Add(this.btnSave);
            this.groupControlSinggleChoice.Controls.Add(this.label7);
            this.groupControlSinggleChoice.Controls.Add(this.label8);
            this.groupControlSinggleChoice.Controls.Add(this.txtContent);
            this.groupControlSinggleChoice.Controls.Add(this.txtChoice);
            this.groupControlSinggleChoice.Location = new System.Drawing.Point(3, 1);
            this.groupControlSinggleChoice.Name = "groupControlSinggleChoice";
            this.groupControlSinggleChoice.Size = new System.Drawing.Size(1056, 531);
            this.groupControlSinggleChoice.TabIndex = 22;
            this.groupControlSinggleChoice.Text = "Câu hỏi ghép hợp";
            // 
            // txtAnswer
            // 
            this.txtAnswer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAnswer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAnswer.Location = new System.Drawing.Point(120, 350);
            this.txtAnswer.Name = "txtAnswer";
            this.txtAnswer.Size = new System.Drawing.Size(915, 34);
            this.txtAnswer.TabIndex = 73;
            this.txtAnswer.Text = "";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(14, 359);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 72;
            this.label2.Text = "Đáp án :";
            // 
            // btnDeleteChoice1
            // 
            this.btnDeleteChoice1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteChoice1.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteChoice1.Image")));
            this.btnDeleteChoice1.Location = new System.Drawing.Point(807, 127);
            this.btnDeleteChoice1.Name = "btnDeleteChoice1";
            this.btnDeleteChoice1.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteChoice1.TabIndex = 71;
            this.btnDeleteChoice1.Text = "Xóa";
            this.btnDeleteChoice1.Click += new System.EventHandler(this.btnDeleteChoice1_Click);
            // 
            // btnEditChoice1
            // 
            this.btnEditChoice1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditChoice1.Image = ((System.Drawing.Image)(resources.GetObject("btnEditChoice1.Image")));
            this.btnEditChoice1.Location = new System.Drawing.Point(726, 127);
            this.btnEditChoice1.Name = "btnEditChoice1";
            this.btnEditChoice1.Size = new System.Drawing.Size(75, 23);
            this.btnEditChoice1.TabIndex = 70;
            this.btnEditChoice1.Text = "Sửa";
            this.btnEditChoice1.Click += new System.EventHandler(this.btnEditChoice1_Click);
            // 
            // btnAddChoice1
            // 
            this.btnAddChoice1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddChoice1.Image = ((System.Drawing.Image)(resources.GetObject("btnAddChoice1.Image")));
            this.btnAddChoice1.Location = new System.Drawing.Point(645, 127);
            this.btnAddChoice1.Name = "btnAddChoice1";
            this.btnAddChoice1.Size = new System.Drawing.Size(75, 23);
            this.btnAddChoice1.TabIndex = 69;
            this.btnAddChoice1.Text = "Thêm";
            this.btnAddChoice1.Click += new System.EventHandler(this.btnAddChoice1_Click);
            // 
            // lvChoice1
            // 
            this.lvChoice1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvChoice1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.lvChoice1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lvChoice1.FullRowSelect = true;
            this.lvChoice1.GridLines = true;
            this.lvChoice1.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvChoice1.Location = new System.Drawing.Point(644, 156);
            this.lvChoice1.Name = "lvChoice1";
            this.lvChoice1.Size = new System.Drawing.Size(392, 188);
            this.lvChoice1.TabIndex = 68;
            this.lvChoice1.UseCompatibleStateImageBehavior = false;
            this.lvChoice1.View = System.Windows.Forms.View.Details;
            this.lvChoice1.Click += new System.EventHandler(this.lvChoice1_Click);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Phương án cột B";
            this.columnHeader1.Width = 385;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(597, 97);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 67;
            this.label1.Text = "Cột B :";
            // 
            // txtChoice1
            // 
            this.txtChoice1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtChoice1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtChoice1.Location = new System.Drawing.Point(645, 87);
            this.txtChoice1.Name = "txtChoice1";
            this.txtChoice1.Size = new System.Drawing.Size(391, 34);
            this.txtChoice1.TabIndex = 66;
            this.txtChoice1.Text = "";
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnDeleteVideo);
            this.groupBox2.Controls.Add(this.btnDeleteAudio);
            this.groupBox2.Controls.Add(this.btnDeletePicture);
            this.groupBox2.Controls.Add(this.btnViewVideo);
            this.groupBox2.Controls.Add(this.btnViewAudio);
            this.groupBox2.Controls.Add(this.btnViewPicture);
            this.groupBox2.Controls.Add(this.txtAudio);
            this.groupBox2.Controls.Add(this.txtVideo);
            this.groupBox2.Controls.Add(this.txtPicture);
            this.groupBox2.Controls.Add(this.btnChooseVideo);
            this.groupBox2.Controls.Add(this.btnChooseAudio);
            this.groupBox2.Controls.Add(this.btnChoosePicture);
            this.groupBox2.Location = new System.Drawing.Point(18, 384);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1018, 110);
            this.groupBox2.TabIndex = 47;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Media";
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(589, 504);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 60;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // ucMatching
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControlSinggleChoice);
            this.Name = "ucMatching";
            this.Size = new System.Drawing.Size(1062, 535);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlSinggleChoice)).EndInit();
            this.groupControlSinggleChoice.ResumeLayout(false);
            this.groupControlSinggleChoice.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.SimpleButton btnSave;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RichTextBox txtContent;
        private System.Windows.Forms.RichTextBox txtChoice;
        private DevExpress.XtraEditors.SimpleButton btnDeleteVideo;
        private DevExpress.XtraEditors.SimpleButton btnDeleteAudio;
        private DevExpress.XtraEditors.SimpleButton btnDeletePicture;
        private DevExpress.XtraEditors.SimpleButton btnViewVideo;
        private DevExpress.XtraEditors.SimpleButton btnViewAudio;
        private System.Windows.Forms.ColumnHeader colChoice;
        private DevExpress.XtraEditors.SimpleButton btnAddChoice;
        private System.Windows.Forms.ListView lvChoice;
        private DevExpress.XtraEditors.SimpleButton btnViewPicture;
        private System.Windows.Forms.TextBox txtAudio;
        private System.Windows.Forms.TextBox txtVideo;
        private System.Windows.Forms.TextBox txtPicture;
        private DevExpress.XtraEditors.SimpleButton btnChooseVideo;
        private DevExpress.XtraEditors.SimpleButton btnChooseAudio;
        private DevExpress.XtraEditors.SimpleButton btnChoosePicture;
        private DevExpress.XtraEditors.SimpleButton btnDeleteChoice;
        private DevExpress.XtraEditors.SimpleButton btnEditChoice;
        private DevExpress.XtraEditors.GroupControl groupControlSinggleChoice;
        private DevExpress.XtraEditors.SimpleButton btnDeleteChoice1;
        private DevExpress.XtraEditors.SimpleButton btnEditChoice1;
        private DevExpress.XtraEditors.SimpleButton btnAddChoice1;
        private System.Windows.Forms.ListView lvChoice1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox txtChoice1;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private System.Windows.Forms.RichTextBox txtAnswer;
        private System.Windows.Forms.Label label2;

    }
}
