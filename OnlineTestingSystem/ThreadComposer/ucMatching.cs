﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml;
using System.IO;

namespace ThreadComposer
{
    public partial class ucMatching : UserControl
    {
        FormMain main = null;
        public ucMatching(FormMain main)
        {
            InitializeComponent();
            //nếu user nhấp item trong list câu hỏi ở FormMain
            if (FormMain.tabName == "View" && FormMain.lvQuestionClick)
            {
                Reset();

                //load dữ liệu câu hỏi được nhấp trên list câu hỏi vào cho ucMultiChoice
                LoadData();
            }
            else
            {
                if (FormMain.tabName == "Add" && FormMain.lvQuestionClick)
                {
                    Reset();

                    //load dữ liệu câu hỏi được nhấp trên list câu hỏi vào cho ucMultiChoice
                    LoadData();
                }
            }
            this.main = main;
        }

        //Lấy tên file từ FormMain
        string FileName = FormMain.FileName;

        //Lấy tên file open từ FormMain
        string FileNameOpen = FormMain.FileNameOpen;
        //Các biến lưu đường dẫn của media
        static string PictureURL = "";
        static string AudioURL = "";
        static string VideoURL = "";

        //lấy vị trí phương án được chọn trên lvChoice
        int choiceSelected;
        int choice1Selected;
        int indexEdit;
        int indexAdd = 65;
        int indexEdit1;
        int indexAdd1 = 1;

        bool updateQuestionSuccess = false;
        bool xoa = false;

        //Reset lại các item trên ucMultiChoice
        public void Reset()
        {
            txtContent.ResetText();
            txtChoice.ResetText();
            txtChoice1.ResetText();
            txtAnswer.ResetText();
            //txtAnswer.ResetText();
            PictureURL = "";
            AudioURL = "";
            VideoURL = "";
            btnViewAudio.Enabled = false;
            btnViewPicture.Enabled = false;
            btnViewVideo.Enabled = false;
            btnDeleteAudio.Enabled = false;
            btnDeletePicture.Enabled = false;
            btnDeleteVideo.Enabled = false;
            txtAudio.ResetText();
            txtPicture.ResetText();
            txtVideo.ResetText();
            txtContent.Focus();
            btnEditChoice.Enabled = false;
            btnDeleteChoice.Enabled = false;
            btnAddChoice.Enabled = true;
            lvChoice.Items.Clear();

            btnEditChoice1.Enabled = false;
            btnDeleteChoice1.Enabled = false;
            btnAddChoice1.Enabled = true;
            lvChoice1.Items.Clear();
            xoa = false;

           // indexEdit ;
            
        }
        //ghi đến file nếu thêm câu hỏi
        private void Write()
        {
            List<string> choice = new List<string>();
            List<string> choice1 = new List<string>();
            for (int i = 0; i < lvChoice.Items.Count; i++)
            {
                choice.Add(lvChoice.Items[i].SubItems[0].Text.Substring(0, 1));
            }
            for (int i = 0; i < lvChoice1.Items.Count; i++)
            {
                choice1.Add(lvChoice1.Items[i].SubItems[0].Text.Substring(1, 1));
            }

            if (choice.Count() < choice1.Count())
            {
                int choiceCount = 0;
                int choice1Count = 0;
                for (int i = 0; i < txtAnswer.Text.Length - 1; i = i + 2)
                {
                    for (int j = 0; j < choice.Count(); j++)
                    {
                        if (txtAnswer.Text[i].ToString() == choice[j])
                        {
                            choiceCount++;
                        }
                    }
                }

                for (int i = 1; i < txtAnswer.Text.Length; i = i + 2)
                {
                    for (int j = 0; j < choice1.Count(); j++)
                    {
                        if (txtAnswer.Text[i].ToString() == choice1[j])
                        {
                            choice1Count++;
                        }
                    }
                }
                if (choiceCount != choice1Count || choiceCount != choice.Count())
                {
                    MessageBox.Show("Câu trả lời không khớp với tên phương án hoặc đáp án không đủ các phương án", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    updateQuestionSuccess = false;
                    return;
                }
                else
                {
                    CreateElementAdd();
                }
            }
            else 
            {
                int choiceCount = 0;
                int choice1Count = 0;
                for (int i = 0; i < txtAnswer.Text.Length - 1; i = i + 2)
                {
                    for (int j = 0; j < choice.Count(); j++)
                    {
                        if (txtAnswer.Text[i].ToString() == choice[j])
                        {
                            choiceCount++;
                        }
                    }
                }

                for (int i = 1; i < txtAnswer.Text.Length; i = i + 2)
                {
                    for (int j = 0; j < choice1.Count(); j++)
                    {
                        if (txtAnswer.Text[i].ToString() == choice1[j])
                        {
                            choice1Count++;
                        }
                    }
                }
                if (choiceCount != choice1Count || choice1Count != choice1.Count())
                {
                    MessageBox.Show("Câu trả lời không khớp với tên phương án hoặc đáp án không đủ các phương án", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    updateQuestionSuccess = false;
                    return;
                }
                else
                {
                    CreateElementAdd();
                }
            }
        }

        public void CreateElementAdd()
        {
            //Load file xml để lưu câu hỏi được thêm vào
            FormMain.xdoc.Load(FileName);

            //tạo thẻ <Question> chứa thông tin câu hỏi
            XmlElement question = FormMain.xdoc.CreateElement("Question");
            FormMain.xdoc.DocumentElement.PrependChild(question);
            //thêm thẻ <Question> vào trong thẻ <Thread>
            FormMain.xdoc.ChildNodes.Item(0).AppendChild(question);

            // tạo thẻ <ID> chứa ID câu hỏi
            XmlElement id = FormMain.xdoc.CreateElement("ID");
            XmlText idText = FormMain.xdoc.CreateTextNode(Guid.NewGuid().ToString());
            id.AppendChild(idText);
            //thêm thẻ <ID> vào thẻ <Question>
            question.PrependChild(id);

            // tạo thẻ <Type> chứa loại câu hỏi
            XmlElement type = FormMain.xdoc.CreateElement("Type");
            XmlText typeText = FormMain.xdoc.CreateTextNode("Matching");
            type.AppendChild(typeText);
            //thêm thẻ <Type> vào thẻ <Question> sau thẻ <ID>
            question.InsertAfter(type, id);

            // tạo thẻ <Content> nội dung câu hỏi
            XmlElement content = FormMain.xdoc.CreateElement("Content");
            XmlText contentText = FormMain.xdoc.CreateTextNode(txtContent.Text.ToString());
            content.AppendChild(contentText);
            //thêm thẻ <Content> vào thẻ <Question> sau thẻ <Type>
            question.InsertAfter(content, type);


            // tạo thẻ <Choices> chứa mảng các phương án
            XmlElement choices = FormMain.xdoc.CreateElement("Choices");
            //thêm thẻ <Choices> vào thẻ <Question> sau thẻ <Content>
            question.InsertAfter(choices, content);

            for (int i = lvChoice.Items.Count - 1; i >= 0; i--)
            {
                // tạo thẻ <Choice> chứa phương án
                XmlElement choiceElement = FormMain.xdoc.CreateElement("Choice");
                XmlText choiceText = FormMain.xdoc.CreateTextNode(lvChoice.Items[i].Text);
                choiceElement.AppendChild(choiceText);
                choices.PrependChild(choiceElement);

            }


            // tạo thẻ <Choices1> chứa mảng các phương án 2 dùng cho câu hỏi matching
            XmlElement choices1 = FormMain.xdoc.CreateElement("Choices1");
            //thêm thẻ <Choices1> vào thẻ <Question> sau thẻ <Choices>
            question.InsertAfter(choices1, choices);

            for (int i = lvChoice1.Items.Count - 1; i >= 0; i--)
            {
                // tạo thẻ <Choice> chứa phương án
                XmlElement choice1Element = FormMain.xdoc.CreateElement("Choice");
                XmlText choice1Text = FormMain.xdoc.CreateTextNode(lvChoice1.Items[i].Text);
                choice1Element.AppendChild(choice1Text);
                choices1.PrependChild(choice1Element);

            }

            // tạo thẻ <Answer> chứa đáp án
            XmlElement answer = FormMain.xdoc.CreateElement("Answer");
            XmlText answerText = FormMain.xdoc.CreateTextNode(txtAnswer.Text);
            answer.AppendChild(answerText);
            //thêm thẻ <Answer> vào thẻ <Question> sau thẻ <Choices1>
            question.InsertAfter(answer, choices1);

            // tạo thẻ <Picture> chứa ảnh của câu hỏi
            XmlElement picture = FormMain.xdoc.CreateElement("Picture");
            XmlText pictureText = FormMain.xdoc.CreateTextNode(PictureURL.ToString().Equals("") ? "" : UserPicture.ImageToBase64(PictureURL));
            picture.AppendChild(pictureText);
            //thêm thẻ <Picture> vào thẻ <Question> sau thẻ <Answer>
            question.InsertAfter(picture, answer);

            // tạo thẻ <Audio> chứa đường dẫn audio của câu hỏi
            XmlElement audio = FormMain.xdoc.CreateElement("Audio");
            XmlText audioText = FormMain.xdoc.CreateTextNode(txtAudio.Text.ToString());
            audio.AppendChild(audioText);
            //thêm thẻ <Audio> vào thẻ <Question> sau thẻ <Picture>
            question.InsertAfter(audio, picture);

            // tạo thẻ <Video> chứa đường dẫn video của câu hỏi
            XmlElement video = FormMain.xdoc.CreateElement("Video");
            XmlText videoText = FormMain.xdoc.CreateTextNode(txtVideo.Text.ToString());
            video.AppendChild(videoText);
            //thêm thẻ <Video> vào thẻ <Question> sau thẻ <Audio>
            question.InsertAfter(video, audio);

            //Lưu nội dung câu hỏi được thêm đến file
            FormMain.xdoc.Save(FileName);
            updateQuestionSuccess = true;
        }

        //ghi đến file nếu sửa câu hỏi
        private void Write(Question q, XmlDocument xdocument)
        {
            // load nội dung file xml được import
            if (FormMain.newClick)
                xdocument.Load(FileName);
            else
                xdocument.Load(FileNameOpen);

            // tạo thẻ <Question>
            XmlElement question = xdocument.CreateElement("Question");
            xdocument.DocumentElement.PrependChild(question);
            xdocument.ChildNodes.Item(0).AppendChild(question);

            // tạo thẻ <ID>
            XmlElement id = xdocument.CreateElement("ID");
            XmlText idText = xdocument.CreateTextNode(Guid.NewGuid().ToString());
            id.AppendChild(idText);
            question.PrependChild(id);

            // tạo thẻ <Type>
            XmlElement type = xdocument.CreateElement("Type");
            XmlText typeText = xdocument.CreateTextNode(q._Type);
            type.AppendChild(typeText);
            question.InsertAfter(type, id);

            // tạo thẻ <Content>
            XmlElement content = xdocument.CreateElement("Content");
            XmlText contentText = xdocument.CreateTextNode(q.Content);
            content.AppendChild(contentText);
            question.InsertAfter(content, type);


            // tạo thẻ <Choices>
            XmlElement choices = xdocument.CreateElement("Choices");
            question.InsertAfter(choices, content);


            for (int i = q.Choices.Count() - 1; i >= 0; i--)
            {
                // tạo thẻ <Choice>
                XmlElement choice = xdocument.CreateElement("Choice");
                XmlText choiceText = xdocument.CreateTextNode(q.Choices[i]._Choice);
                choice.AppendChild(choiceText);
                choices.PrependChild(choice);
            }

            // tạo thẻ <Choices1> cho câu hỏi Matching
            XmlElement choices1 = xdocument.CreateElement("Choices1");
            question.InsertAfter(choices1, choices);

            for (int i = q.Choices1.Count() - 1; i >= 0; i--)
            {
                // tạo thẻ <Choice>
                XmlElement choice1 = xdocument.CreateElement("Choice");
                XmlText choice1Text = xdocument.CreateTextNode(q.Choices1[i]._Choice);
                choice1.AppendChild(choice1Text);
                choices1.PrependChild(choice1);
            }


            // tạo thẻ <Answer>
            XmlElement answer = xdocument.CreateElement("Answer");
            XmlText answerText = xdocument.CreateTextNode(q.Answer);
            answer.AppendChild(answerText);
            question.InsertAfter(answer, choices1);

            // tạo thẻ <Picture>
            XmlElement picture = xdocument.CreateElement("Picture");
            XmlText pictureText = xdocument.CreateTextNode(q.Picture);
            picture.AppendChild(pictureText);
            question.InsertAfter(picture, answer);

            // tạo thẻ <Audio>
            XmlElement audio = xdocument.CreateElement("Audio");
            XmlText audioText = xdocument.CreateTextNode(q.Audio);
            audio.AppendChild(audioText);
            question.InsertAfter(audio, picture);

            // tạo thẻ <Video>
            XmlElement video = xdocument.CreateElement("Video");
            XmlText videoText = xdocument.CreateTextNode(q.Video);
            video.AppendChild(videoText);
            question.InsertAfter(video, audio);

            if (FormMain.newClick)
                xdocument.Load(FileName);
            else
                xdocument.Load(FileNameOpen);
        }

        public void WriteUpdate()
        {
            //update nội dung câu hỏi đã sửa
            if (!xoa)
                updateQuestion();

            //Tạo 1 file xml mới
            XmlDocument xdocument = new XmlDocument();

            //tạo declaration
            xdocument.CreateXmlDeclaration("1.0", "utf-8", null);
            XmlNode root = xdocument.CreateNode(XmlNodeType.XmlDeclaration, "", "");

            //tạo node Thread
            XmlElement rootElement = xdocument.CreateElement("Thread");

            //thêm node đến document
            xdocument.AppendChild(rootElement);
            if (FormMain.newClick)
                xdocument.Save(FileName);
            else
                xdocument.Save(FileNameOpen);

            //Load nội dung câu hỏi vào lại file xml
            foreach (var q in FormMain.thread)
            {
                Write(q, xdocument);
            }
        }

        private void btnAddChoice_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtChoice.Text != "")
                {

                    //nếu sửa câu hỏi
                    if (FormMain.tabName == "View")
                    {
                        bool existChoice = false;
                        for (int i = 0; i < lvChoice.Items.Count; i++)
                        {
                            string str = lvChoice.Items[i].SubItems[0].Text;
                            if (str.Substring(3, str.Length - 3) == txtChoice.Text.Trim())
                            {
                                existChoice = true;
                                break;
                            }

                        }
                        if (existChoice)
                        {
                            MessageBox.Show("Phương án này đã có rồi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            indexEdit++;
                            lvChoice.Items.Add(Convert.ToChar(indexEdit).ToString() + ". " + txtChoice.Text.ToString());
                        }
                    }
                    //nếu thêm câu hỏi
                    else
                    {
                        bool existChoice = false;
                        for (int i = 0; i < lvChoice.Items.Count; i++)
                        {

                            string str = lvChoice.Items[i].SubItems[0].Text;
                            if (str.Substring(3, str.Length - 3) == txtChoice.Text.Trim())
                            {
                                existChoice = true;
                                break;
                            }
                        }

                        if (existChoice)
                        {
                            MessageBox.Show("Phương án này đã có rồi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            lvChoice.Items.Add(Convert.ToChar(indexAdd).ToString() + ". " + txtChoice.Text.ToString());
                            indexAdd++;
                        }

                    }

                    txtChoice.ResetText();
                    txtChoice.Focus();

                }
                else
                    MessageBox.Show("Vui lòng nhập nội dung phương án !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Thêm phương án không thành công !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnEditChoice_Click(object sender, EventArgs e)
        {
            lvChoice.Items[choiceSelected].Text = txtChoice.Text.ToString();
            int index = 65;

            for (int i = 0; i < lvChoice.Items.Count; i++)
            {
                if (i != choiceSelected)
                {

                    lvChoice.Items[i].SubItems[0].Text = Convert.ToChar(index).ToString() + ". " + lvChoice.Items[i].SubItems[0].Text.Substring(3, lvChoice.Items[i].SubItems[0].Text.Length - 3);
                    index++;
                }
                else
                {
                    lvChoice.Items[i].SubItems[0].Text = Convert.ToChar(index).ToString() + ". " + txtChoice.Text.Trim();
                    index++;
                }
            }
            indexEdit = index - 1;
        }

        private void btnDeleteChoice_Click(object sender, EventArgs e)
        {
            lvChoice.Items.RemoveAt(choiceSelected);
            int index = 65;

            for (int i = 0; i < lvChoice.Items.Count; i++)
            {

                lvChoice.Items[i].SubItems[0].Text = Convert.ToChar(index).ToString() + ". " + lvChoice.Items[i].SubItems[0].Text.Substring(3, lvChoice.Items[i].SubItems[0].Text.Length - 3);
                index++;
            }
            indexEdit = index - 1;
            txtChoice.ResetText();
            txtChoice.Focus();
        }

        private void btnAddChoice1_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtChoice1.Text != "")
                {

                    //nếu sửa câu hỏi
                    if (FormMain.tabName == "View")
                    {
                        bool existChoice1 = false;
                        for (int i = 0; i < lvChoice1.Items.Count; i++)
                        {
                            string str = lvChoice1.Items[i].SubItems[0].Text;
                            if (str.Substring(5, str.Length - 5) == txtChoice1.Text.Trim())
                            {
                                existChoice1 = true;
                                break;
                            }

                        }
                        if (existChoice1)
                        {
                            MessageBox.Show("Phương án này đã có rồi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            indexEdit1++;
                            lvChoice1.Items.Add("(" + indexEdit1.ToString() + "). " + txtChoice1.Text.ToString());
                        }
                    }
                    //nếu thêm câu hỏi
                    else
                    {
                        bool existChoice1 = false;
                        for (int i = 0; i < lvChoice1.Items.Count; i++)
                        {

                            string str = lvChoice1.Items[i].SubItems[0].Text;
                            if (str.Substring(5, str.Length - 5) == txtChoice1.Text.Trim())
                            {
                                existChoice1 = true;
                                break;
                            }
                        }

                        if (existChoice1)
                        {
                            MessageBox.Show("Phương án này đã có rồi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            lvChoice1.Items.Add("(" + indexAdd1.ToString() + "). " + txtChoice1.Text.ToString());
                            indexAdd1++;
                        }

                    }

                    txtChoice1.ResetText();
                    txtChoice1.Focus();

                }
                else
                    MessageBox.Show("Vui lòng nhập nội dung phương án !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Thêm phương án không thành công !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void btnEditChoice1_Click(object sender, EventArgs e)
        {
            lvChoice1.Items[choice1Selected].Text = txtChoice1.Text.ToString();
            int index = 65;

            for (int i = 0; i < lvChoice1.Items.Count; i++)
            {
                if (i != choice1Selected)
                {

                    lvChoice1.Items[i].SubItems[0].Text = "(" + index.ToString() + "). " + lvChoice1.Items[i].SubItems[0].Text.Substring(5, lvChoice1.Items[i].SubItems[0].Text.Length - 5);
                    index++;
                }
                else
                {
                    lvChoice1.Items[i].SubItems[0].Text = "(" + index.ToString() + "). " + txtChoice1.Text.Trim();
                    index++;
                }
            }
            indexEdit1 = index - 1;
        }

        private void btnDeleteChoice1_Click(object sender, EventArgs e)
        {
            lvChoice1.Items.RemoveAt(choice1Selected);
            int index = 1;

            for (int i = 0; i < lvChoice1.Items.Count; i++)
            {

                lvChoice1.Items[i].SubItems[0].Text = "(" + index.ToString() + "). " + lvChoice1.Items[i].SubItems[0].Text.Substring(5, lvChoice1.Items[i].SubItems[0].Text.Length - 5);
                index++;
            }
            indexEdit1 = index - 1;
            txtChoice1.ResetText();
            txtChoice1.Focus();
        }

        private void btnChoosePicture_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "Chọn hình ảnh (*.jpg)|*.jpg*";
                ofd.ShowDialog();
                string fileName = ofd.FileName;
                PictureURL = fileName;
                if (string.IsNullOrEmpty(fileName))
                    return;
                txtPicture.Text = fileName;
                btnChoosePicture.BackColor = Color.Red;
                btnViewPicture.Enabled = true;
                btnDeletePicture.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Vui lòng chọn file ảnh nhé !");
            }
        }

        private void btnChooseAudio_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Chọn âm thanh (*.mp3)|*.mp3*";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                txtAudio.Text = ofd.FileName;
                AudioURL = ofd.FileName;
                btnViewAudio.Enabled = true;
                btnDeleteAudio.Enabled = true;
            }
        }

        private void btnChooseVideo_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Chọn video (*.mp4*)|*.mp4*";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                txtVideo.Text = ofd.FileName;
                VideoURL = ofd.FileName;
                btnViewVideo.Enabled = true;
                btnDeleteVideo.Enabled = true;
            }
        }

        private void btnViewPicture_Click(object sender, EventArgs e)
        {
            try
            {
                Picture media = new Picture();
                media.ShowPicture(txtPicture.Text);
                btnDeletePicture.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Không tìm thấy file hoặc file sai định dạng!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDeletePicture_Click(object sender, EventArgs e)
        {
            txtPicture.ResetText();
            btnViewPicture.Enabled = false;
            btnDeletePicture.Enabled = false;
        }

        private void btnViewAudio_Click(object sender, EventArgs e)
        {
            try
            {
                Audio audio = new Audio();
                audio.ShowAudio(txtAudio.Text);
                audio.Show();
                btnDeleteAudio.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Không tìm thấy file hoặc file sai định dạng!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDeleteAudio_Click(object sender, EventArgs e)
        {
            txtAudio.ResetText();
            btnViewAudio.Enabled = false;
            btnDeleteAudio.Enabled = false;
        }

        private void btnViewVideo_Click(object sender, EventArgs e)
        {
            try
            {
                Video video = new Video();
                video.ShowVideo(txtVideo.Text);
                video.Show();
                btnDeleteVideo.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Không tìm thấy file hoặc file sai định dạng!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnDeleteVideo_Click(object sender, EventArgs e)
        {
            txtVideo.ResetText();
            btnViewVideo.Enabled = false;
            btnDeleteVideo.Enabled = false;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtContent.Text == "" || txtAnswer.Text == "" || lvChoice.Items.Count == 0 || lvChoice1.Items.Count == 0)
                {
                    MessageBox.Show("Vui lòng nhập đầy đủ thông tin trước khi lưu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (FormMain.tabName == "View" && FormMain.lvQuestionClick)
                    {

                        WriteUpdate();
                        if (FormMain.newClick)
                        {
                            main.ReadXmlFile(FileName);
                        }
                        else
                        {
                            main.ReadXmlFile(FileNameOpen);
                        }
                     //   main.LvQuestionList(FormMain.thread);
                    }
                    else
                    {
                        if (FormMain.tabName == "Add" && FormMain.lvQuestionClick)
                        {
                            Write();
                            if (FormMain.newClick)
                                main.ReadXmlFile(FileName);
                            else
                                main.ReadXmlFile(FileNameOpen);
                            //main.LvQuestionListAdd(FormMain.thread);
                        }
                        else
                        {
                            Write();
                            main.ReadXmlFile(FileName);
                          //  main.LvQuestionListAdd(FormMain.thread);
                        }
                    }
                }

                indexAdd = 65;
                indexEdit = 0;

                indexAdd1 = 1;
                indexEdit1 = 0;

                if (updateQuestionSuccess)
                    Reset();
            }
            catch
            {
                return;
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            if (FormMain.tabName == "View" && FormMain.lvQuestionClick)
            {
                FormMain.thread.RemoveAt(FormMain.currentQuestion);

                xoa = true;
                WriteUpdate();
                //main.LvQuestionList(FormMain.thread);
                Reset();
                FormMain.lvQuestionClick = false;
            }
            else
                if (FormMain.tabName == "Add" && FormMain.lvQuestionClick)
                {
                    FormMain.thread.RemoveAt(FormMain.currentQuestion);

                    xoa = true;
                    WriteUpdate();
                  //  main.LvQuestionListAdd(FormMain.thread);
                    Reset();
                    FormMain.lvQuestionClick = false;
                }
                else
                {
                    MessageBox.Show("Câu trả hỏi chưa được lưu. Không thể xóa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    FormMain.lvQuestionClick = false;
                    return;
                }
        }

        //Upload hình ảnh
        public void UploadPicture()
        {
            try
            {
                string path = Application.StartupPath.Substring(0, Application.StartupPath.Length - 10);
                string filename = Path.GetFileName(txtPicture.Text);
                File.Copy(txtPicture.Text, path + "\\Pictures\\" + filename);
                //pictureBox.ImageLocation = path + "\\Pictures\\" + filename;
                MessageBox.Show("Đã xong");
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi upload hình !", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //Upload âm thanh
        public void UploadAudio()
        {
            try
            {
                string path = Application.StartupPath.Substring(0, Application.StartupPath.Length - 10);
                string filename = Path.GetFileName(txtAudio.Text);
                File.Copy(txtAudio.Text, path + "\\Audios\\" + filename);
                //axAudio.URL = path + "\\Audios\\" + filename;
                MessageBox.Show("Đã xong");
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi upload audio !", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //pload video
        public void UploadVideo()
        {
            try
            {
                string path = Application.StartupPath.Substring(0, Application.StartupPath.Length - 10);
                string filename = Path.GetFileName(txtVideo.Text);
                File.Copy(txtVideo.Text, path + "\\Videos\\" + filename);
                //axVideo.URL = path + "\\Videos\\" + filename;
                MessageBox.Show("Đã xong");
            }
            catch
            {
                MessageBox.Show("Lỗi upload video !", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void lvChoice_Click(object sender, EventArgs e)
        {
            try
            {
                choiceSelected = lvChoice.Items.IndexOf(lvChoice.SelectedItems[0]);
                txtChoice.Text = lvChoice.SelectedItems[0].SubItems[0].Text.Substring(3, lvChoice.SelectedItems[0].SubItems[0].Text.Length - 3);

             //   txtAnswer.Text = lvChoice.SelectedItems[0].SubItems[0].Text;

                btnEditChoice.Enabled = true;
                btnDeleteChoice.Enabled = true;
            }
            catch
            {
                //MessageBox.Show("Chọn phương án trước khi chọn câu trả lời đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void lvChoice1_Click(object sender, EventArgs e)
        {
            try
            {
                choice1Selected = lvChoice1.Items.IndexOf(lvChoice1.SelectedItems[0]);
                txtChoice1.Text = lvChoice1.SelectedItems[0].SubItems[0].Text.Substring(5, lvChoice1.SelectedItems[0].SubItems[0].Text.Length - 5);

             //   txtAnswer.Text = lvChoice1.SelectedItems[0].SubItems[0].Text;

                btnEditChoice1.Enabled = true;
                btnDeleteChoice1.Enabled = true;
            }
            catch
            {
                //MessageBox.Show("Chọn phương án trước khi chọn câu trả lời đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        //Load dữ liệu vào các item khi click câu hỏi trên list câu hỏi - dùng để sửa
        public void LoadData()
        {
            try
            {
                txtContent.Text = FormMain.thread[FormMain.currentQuestion].Content;
                for (int i = 0; i < FormMain.thread[FormMain.currentQuestion].Choices.Count(); i++)
                {
                    lvChoice.Items.Add(FormMain.thread[FormMain.currentQuestion].Choices[i]._Choice);
                }

                for (int i = 0; i < FormMain.thread[FormMain.currentQuestion].Choices1.Count(); i++)
                {
                    lvChoice1.Items.Add(FormMain.thread[FormMain.currentQuestion].Choices1[i]._Choice);
                }
                txtAnswer.Text = FormMain.thread[FormMain.currentQuestion].Answer;

                txtPicture.Text = FormMain.thread[FormMain.currentQuestion].Picture;
                txtAudio.Text = FormMain.thread[FormMain.currentQuestion].Audio;
                txtVideo.Text = FormMain.thread[FormMain.currentQuestion].Video;

                if (txtPicture.Text.Trim() != "")
                {
                    btnViewPicture.Enabled = true;
                    btnDeletePicture.Enabled = true;
                }
                if (txtAudio.Text.Trim() != "")
                {
                    btnViewAudio.Enabled = true;
                    btnDeleteAudio.Enabled = true;
                }
                if (txtVideo.Text.Trim() != "")
                {
                    btnViewVideo.Enabled = true;
                    btnDeleteVideo.Enabled = true;
                }

                int lastIndex = FormMain.thread[FormMain.currentQuestion].Choices.Count() - 1;
                indexEdit = Convert.ToInt32(Convert.ToChar((FormMain.thread[FormMain.currentQuestion].Choices[lastIndex]._Choice).Substring(0, 1)));

                int lastIndex1 = FormMain.thread[FormMain.currentQuestion].Choices1.Count() - 1;
                indexEdit1 = Convert.ToInt32((FormMain.thread[FormMain.currentQuestion].Choices1[lastIndex1]._Choice.Substring(1, 1)));
            }
            catch
            {
                MessageBox.Show("Không load được câu hỏi", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        //cập nhật nội dung câu hỏi khi sửa
        public void updateQuestion()
        {

           // bool existChoice = false;
         //   bool existChoice1 = false;
            List<string> choice = new List<string>();
            List<string> choice1 = new List<string>();
            for (int i = 0; i < lvChoice.Items.Count; i++)
            {
                choice.Add(lvChoice.Items[i].SubItems[0].Text.Substring(0, 1));
            }
            for (int i = 0; i < lvChoice1.Items.Count; i++)
            {
                choice1.Add(lvChoice1.Items[i].SubItems[0].Text.Substring(1, 1));
            }
            
            if (choice.Count() < choice1.Count())
            {
                int choiceCount = 0;
                int choice1Count = 0;
                for (int i = 0; i < txtAnswer.Text.Length-1; i=i+2)
                {
                    for(int j=0; j<choice.Count(); j++)
                    {
                        if (txtAnswer.Text[i].ToString() == choice[j])
                        {
                            choiceCount++;
                        }
                    }
                }

                for (int i = 1; i < txtAnswer.Text.Length; i=i+2)
                {
                    for (int j = 0; j < choice1.Count(); j++)
                    {
                        if (txtAnswer.Text[i].ToString() == choice1[j])
                        {
                            choice1Count++;
                        }
                    }
                }
                if (choiceCount != choice1Count || choiceCount!=choice.Count())
                {
                    MessageBox.Show("Câu trả lời không khớp với tên phương án hoặc đáp án không đủ các phương án", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    updateQuestionSuccess = false;
                    return;
                }
                else
                {
                    xoa = false;
                    FormMain.thread[FormMain.currentQuestion].Content = txtContent.Text;
                    FormMain.thread[FormMain.currentQuestion].Choices.Clear();
                    FormMain.thread[FormMain.currentQuestion].Choices1.Clear();
                    FormMain.thread[FormMain.currentQuestion].Answer = txtAnswer.Text;
                    FormMain.thread[FormMain.currentQuestion].Picture = txtPicture.Text;
                    FormMain.thread[FormMain.currentQuestion].Audio = txtAudio.Text;
                    FormMain.thread[FormMain.currentQuestion].Video = txtVideo.Text;
                    updateQuestionSuccess = true;
                }

            }
            else
            {
                int choiceCount = 0;
                int choice1Count = 0;
                for (int i = 0; i < txtAnswer.Text.Length - 1; i = i + 2)
                {
                    for (int j = 0; j < choice.Count(); j++)
                    {
                        if (txtAnswer.Text[i].ToString() == choice[j])
                        {
                            choiceCount++;
                        }
                    }
                }

                for (int i = 1; i < txtAnswer.Text.Length; i = i + 2)
                {
                    for (int j = 0; j < choice1.Count(); j++)
                    {
                        if (txtAnswer.Text[i].ToString() == choice1[j])
                        {
                            choice1Count++;
                        }
                    }
                }
                if (choiceCount != choice1Count || choice1Count != choice1.Count())
                {
                    MessageBox.Show("Câu trả lời không khớp với tên phương án hoặc đáp án không đủ các phương án", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    updateQuestionSuccess = false;
                    return;
                }
                else
                {
                    xoa = false;
                    FormMain.thread[FormMain.currentQuestion].Content = txtContent.Text;
                    FormMain.thread[FormMain.currentQuestion].Choices.Clear();
                    FormMain.thread[FormMain.currentQuestion].Choices1.Clear();
                    FormMain.thread[FormMain.currentQuestion].Answer = txtAnswer.Text;
                    FormMain.thread[FormMain.currentQuestion].Picture = txtPicture.Text;
                    FormMain.thread[FormMain.currentQuestion].Audio = txtAudio.Text;
                    FormMain.thread[FormMain.currentQuestion].Video = txtVideo.Text;
                    updateQuestionSuccess = true;
                }
            }
        }
    }
}
