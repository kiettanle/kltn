﻿namespace ThreadComposer
{
    partial class ucMultiChoice
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucMultiChoice));
            this.groupControlSinggleChoice = new DevExpress.XtraEditors.GroupControl();
            this.btnDeleteChoice = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditChoice = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddChoice = new DevExpress.XtraEditors.SimpleButton();
            this.lvChoice = new System.Windows.Forms.ListView();
            this.colChoice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnDeleteVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteAudio = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeletePicture = new DevExpress.XtraEditors.SimpleButton();
            this.btnViewVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btnViewAudio = new DevExpress.XtraEditors.SimpleButton();
            this.btnViewPicture = new DevExpress.XtraEditors.SimpleButton();
            this.txtAudio = new System.Windows.Forms.TextBox();
            this.txtVideo = new System.Windows.Forms.TextBox();
            this.txtPicture = new System.Windows.Forms.TextBox();
            this.btnChooseVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btnChooseAudio = new DevExpress.XtraEditors.SimpleButton();
            this.btnChoosePicture = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtContent = new System.Windows.Forms.RichTextBox();
            this.txtChoice = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlSinggleChoice)).BeginInit();
            this.groupControlSinggleChoice.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControlSinggleChoice
            // 
            this.groupControlSinggleChoice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControlSinggleChoice.Controls.Add(this.btnDeleteChoice);
            this.groupControlSinggleChoice.Controls.Add(this.btnEditChoice);
            this.groupControlSinggleChoice.Controls.Add(this.btnAddChoice);
            this.groupControlSinggleChoice.Controls.Add(this.lvChoice);
            this.groupControlSinggleChoice.Controls.Add(this.groupBox2);
            this.groupControlSinggleChoice.Controls.Add(this.btnDelete);
            this.groupControlSinggleChoice.Controls.Add(this.btnSave);
            this.groupControlSinggleChoice.Controls.Add(this.label7);
            this.groupControlSinggleChoice.Controls.Add(this.label8);
            this.groupControlSinggleChoice.Controls.Add(this.txtContent);
            this.groupControlSinggleChoice.Controls.Add(this.txtChoice);
            this.groupControlSinggleChoice.Location = new System.Drawing.Point(0, 0);
            this.groupControlSinggleChoice.Name = "groupControlSinggleChoice";
            this.groupControlSinggleChoice.Size = new System.Drawing.Size(882, 531);
            this.groupControlSinggleChoice.TabIndex = 21;
            this.groupControlSinggleChoice.Text = "Câu hỏi nhiều lựa chọn";
            // 
            // btnDeleteChoice
            // 
            this.btnDeleteChoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteChoice.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteChoice.Image")));
            this.btnDeleteChoice.Location = new System.Drawing.Point(786, 92);
            this.btnDeleteChoice.Name = "btnDeleteChoice";
            this.btnDeleteChoice.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteChoice.TabIndex = 65;
            this.btnDeleteChoice.Text = "Xóa";
            this.btnDeleteChoice.Click += new System.EventHandler(this.btnDeleteChoice_Click);
            // 
            // btnEditChoice
            // 
            this.btnEditChoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditChoice.Image = ((System.Drawing.Image)(resources.GetObject("btnEditChoice.Image")));
            this.btnEditChoice.Location = new System.Drawing.Point(705, 92);
            this.btnEditChoice.Name = "btnEditChoice";
            this.btnEditChoice.Size = new System.Drawing.Size(75, 23);
            this.btnEditChoice.TabIndex = 64;
            this.btnEditChoice.Text = "Sửa";
            this.btnEditChoice.Click += new System.EventHandler(this.btnEditChoice_Click);
            // 
            // btnAddChoice
            // 
            this.btnAddChoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddChoice.Image = ((System.Drawing.Image)(resources.GetObject("btnAddChoice.Image")));
            this.btnAddChoice.Location = new System.Drawing.Point(624, 92);
            this.btnAddChoice.Name = "btnAddChoice";
            this.btnAddChoice.Size = new System.Drawing.Size(75, 23);
            this.btnAddChoice.TabIndex = 63;
            this.btnAddChoice.Text = "Thêm";
            this.btnAddChoice.Click += new System.EventHandler(this.btnAddChoice_Click);
            // 
            // lvChoice
            // 
            this.lvChoice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvChoice.CheckBoxes = true;
            this.lvChoice.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colChoice});
            this.lvChoice.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lvChoice.FullRowSelect = true;
            this.lvChoice.GridLines = true;
            this.lvChoice.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.lvChoice.Location = new System.Drawing.Point(121, 135);
            this.lvChoice.Name = "lvChoice";
            this.lvChoice.Size = new System.Drawing.Size(741, 169);
            this.lvChoice.TabIndex = 62;
            this.lvChoice.UseCompatibleStateImageBehavior = false;
            this.lvChoice.View = System.Windows.Forms.View.Details;
            this.lvChoice.Click += new System.EventHandler(this.lvChoice_Click);
            // 
            // colChoice
            // 
            this.colChoice.Text = "Phương án";
            this.colChoice.Width = 737;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnDeleteVideo);
            this.groupBox2.Controls.Add(this.btnDeleteAudio);
            this.groupBox2.Controls.Add(this.btnDeletePicture);
            this.groupBox2.Controls.Add(this.btnViewVideo);
            this.groupBox2.Controls.Add(this.btnViewAudio);
            this.groupBox2.Controls.Add(this.btnViewPicture);
            this.groupBox2.Controls.Add(this.txtAudio);
            this.groupBox2.Controls.Add(this.txtVideo);
            this.groupBox2.Controls.Add(this.txtPicture);
            this.groupBox2.Controls.Add(this.btnChooseVideo);
            this.groupBox2.Controls.Add(this.btnChooseAudio);
            this.groupBox2.Controls.Add(this.btnChoosePicture);
            this.groupBox2.Location = new System.Drawing.Point(18, 350);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(844, 110);
            this.groupBox2.TabIndex = 47;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Media";
            // 
            // btnDeleteVideo
            // 
            this.btnDeleteVideo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteVideo.Enabled = false;
            this.btnDeleteVideo.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteVideo.Image")));
            this.btnDeleteVideo.Location = new System.Drawing.Point(814, 78);
            this.btnDeleteVideo.Name = "btnDeleteVideo";
            this.btnDeleteVideo.Size = new System.Drawing.Size(23, 23);
            this.btnDeleteVideo.TabIndex = 28;
            this.btnDeleteVideo.Click += new System.EventHandler(this.btnDeleteVideo_Click);
            // 
            // btnDeleteAudio
            // 
            this.btnDeleteAudio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteAudio.Enabled = false;
            this.btnDeleteAudio.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteAudio.Image")));
            this.btnDeleteAudio.Location = new System.Drawing.Point(814, 49);
            this.btnDeleteAudio.Name = "btnDeleteAudio";
            this.btnDeleteAudio.Size = new System.Drawing.Size(23, 23);
            this.btnDeleteAudio.TabIndex = 27;
            this.btnDeleteAudio.Click += new System.EventHandler(this.btnDeleteAudio_Click);
            // 
            // btnDeletePicture
            // 
            this.btnDeletePicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeletePicture.Enabled = false;
            this.btnDeletePicture.Image = ((System.Drawing.Image)(resources.GetObject("btnDeletePicture.Image")));
            this.btnDeletePicture.Location = new System.Drawing.Point(814, 20);
            this.btnDeletePicture.Name = "btnDeletePicture";
            this.btnDeletePicture.Size = new System.Drawing.Size(23, 23);
            this.btnDeletePicture.TabIndex = 26;
            this.btnDeletePicture.Click += new System.EventHandler(this.btnDeletePicture_Click);
            // 
            // btnViewVideo
            // 
            this.btnViewVideo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewVideo.Enabled = false;
            this.btnViewVideo.Image = ((System.Drawing.Image)(resources.GetObject("btnViewVideo.Image")));
            this.btnViewVideo.Location = new System.Drawing.Point(785, 78);
            this.btnViewVideo.Name = "btnViewVideo";
            this.btnViewVideo.Size = new System.Drawing.Size(23, 23);
            this.btnViewVideo.TabIndex = 25;
            this.btnViewVideo.Click += new System.EventHandler(this.btnViewVideo_Click);
            // 
            // btnViewAudio
            // 
            this.btnViewAudio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewAudio.Enabled = false;
            this.btnViewAudio.Image = ((System.Drawing.Image)(resources.GetObject("btnViewAudio.Image")));
            this.btnViewAudio.Location = new System.Drawing.Point(785, 49);
            this.btnViewAudio.Name = "btnViewAudio";
            this.btnViewAudio.Size = new System.Drawing.Size(23, 23);
            this.btnViewAudio.TabIndex = 24;
            this.btnViewAudio.Click += new System.EventHandler(this.btnViewAudio_Click);
            // 
            // btnViewPicture
            // 
            this.btnViewPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewPicture.Enabled = false;
            this.btnViewPicture.Image = ((System.Drawing.Image)(resources.GetObject("btnViewPicture.Image")));
            this.btnViewPicture.Location = new System.Drawing.Point(785, 20);
            this.btnViewPicture.Name = "btnViewPicture";
            this.btnViewPicture.Size = new System.Drawing.Size(23, 23);
            this.btnViewPicture.TabIndex = 23;
            this.btnViewPicture.Click += new System.EventHandler(this.btnViewPicture_Click);
            // 
            // txtAudio
            // 
            this.txtAudio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAudio.Location = new System.Drawing.Point(94, 51);
            this.txtAudio.Name = "txtAudio";
            this.txtAudio.Size = new System.Drawing.Size(685, 21);
            this.txtAudio.TabIndex = 22;
            // 
            // txtVideo
            // 
            this.txtVideo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVideo.Location = new System.Drawing.Point(94, 80);
            this.txtVideo.Name = "txtVideo";
            this.txtVideo.Size = new System.Drawing.Size(685, 21);
            this.txtVideo.TabIndex = 21;
            // 
            // txtPicture
            // 
            this.txtPicture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPicture.Location = new System.Drawing.Point(94, 20);
            this.txtPicture.Name = "txtPicture";
            this.txtPicture.Size = new System.Drawing.Size(685, 21);
            this.txtPicture.TabIndex = 19;
            // 
            // btnChooseVideo
            // 
            this.btnChooseVideo.Image = ((System.Drawing.Image)(resources.GetObject("btnChooseVideo.Image")));
            this.btnChooseVideo.Location = new System.Drawing.Point(7, 78);
            this.btnChooseVideo.Name = "btnChooseVideo";
            this.btnChooseVideo.Size = new System.Drawing.Size(75, 23);
            this.btnChooseVideo.TabIndex = 2;
            this.btnChooseVideo.Text = "Video";
            this.btnChooseVideo.Click += new System.EventHandler(this.btnChooseVideo_Click);
            // 
            // btnChooseAudio
            // 
            this.btnChooseAudio.Image = ((System.Drawing.Image)(resources.GetObject("btnChooseAudio.Image")));
            this.btnChooseAudio.Location = new System.Drawing.Point(7, 49);
            this.btnChooseAudio.Name = "btnChooseAudio";
            this.btnChooseAudio.Size = new System.Drawing.Size(75, 23);
            this.btnChooseAudio.TabIndex = 1;
            this.btnChooseAudio.Text = "Âm thanh";
            this.btnChooseAudio.Click += new System.EventHandler(this.btnChooseAudio_Click);
            // 
            // btnChoosePicture
            // 
            this.btnChoosePicture.Image = ((System.Drawing.Image)(resources.GetObject("btnChoosePicture.Image")));
            this.btnChoosePicture.Location = new System.Drawing.Point(7, 20);
            this.btnChoosePicture.Name = "btnChoosePicture";
            this.btnChoosePicture.Size = new System.Drawing.Size(75, 23);
            this.btnChoosePicture.TabIndex = 0;
            this.btnChoosePicture.Text = "Hình ảnh";
            this.btnChoosePicture.Click += new System.EventHandler(this.btnChoosePicture_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(502, 470);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 60;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.BackgroundImageChanged += new System.EventHandler(this.btnSave_Click);
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(340, 469);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 59;
            this.btnSave.Text = "Lưu";
            this.btnSave.BackgroundImageChanged += new System.EventHandler(this.btnSave_Click);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(45, 97);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 12;
            this.label7.Text = "Phương án :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(14, 48);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 13);
            this.label8.TabIndex = 11;
            this.label8.Text = "Nội dung câu hỏi :";
            // 
            // txtContent
            // 
            this.txtContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtContent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtContent.Location = new System.Drawing.Point(121, 37);
            this.txtContent.Name = "txtContent";
            this.txtContent.Size = new System.Drawing.Size(741, 34);
            this.txtContent.TabIndex = 7;
            this.txtContent.Text = "";
            // 
            // txtChoice
            // 
            this.txtChoice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtChoice.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtChoice.Location = new System.Drawing.Point(121, 87);
            this.txtChoice.Name = "txtChoice";
            this.txtChoice.Size = new System.Drawing.Size(497, 34);
            this.txtChoice.TabIndex = 9;
            this.txtChoice.Text = "";
            // 
            // ucMultiChoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControlSinggleChoice);
            this.Name = "ucMultiChoice";
            this.Size = new System.Drawing.Size(882, 531);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlSinggleChoice)).EndInit();
            this.groupControlSinggleChoice.ResumeLayout(false);
            this.groupControlSinggleChoice.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControlSinggleChoice;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RichTextBox txtContent;
        private System.Windows.Forms.RichTextBox txtChoice;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.SimpleButton btnDeleteVideo;
        private DevExpress.XtraEditors.SimpleButton btnDeleteAudio;
        private DevExpress.XtraEditors.SimpleButton btnDeletePicture;
        private DevExpress.XtraEditors.SimpleButton btnViewVideo;
        private DevExpress.XtraEditors.SimpleButton btnViewAudio;
        private DevExpress.XtraEditors.SimpleButton btnViewPicture;
        private System.Windows.Forms.TextBox txtAudio;
        private System.Windows.Forms.TextBox txtVideo;
        private System.Windows.Forms.TextBox txtPicture;
        private DevExpress.XtraEditors.SimpleButton btnChooseVideo;
        private DevExpress.XtraEditors.SimpleButton btnChooseAudio;
        private DevExpress.XtraEditors.SimpleButton btnChoosePicture;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private System.Windows.Forms.ListView lvChoice;
        private System.Windows.Forms.ColumnHeader colChoice;
        private DevExpress.XtraEditors.SimpleButton btnDeleteChoice;
        private DevExpress.XtraEditors.SimpleButton btnEditChoice;
        private DevExpress.XtraEditors.SimpleButton btnAddChoice;

    }
}
