﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using System.IO;
using System.Xml;

namespace ThreadComposer
{
    public partial class ucMultiChoice : UserControl
    {
        FormMain main = null;

        public ucMultiChoice(FormMain main)
        {
            InitializeComponent();

            //nếu user nhấp item trong list câu hỏi ở FormMain
            if (FormMain.tabName == "View" && FormMain.lvQuestionClick)
            {
                FormMain.lvQuestionClick = true;
                Reset();


                //load dữ liệu câu hỏi được nhấp trên list câu hỏi vào cho ucMultiChoice
                LoadData();
            }
            else
            {
                if (FormMain.tabName == "Add" && FormMain.lvQuestionClick)
                {
                 //   FormMain.lvQuestionClick = true;
                    Reset();

                    //load dữ liệu câu hỏi được nhấp trên list câu hỏi vào cho ucMultiChoice
                    LoadData();
                }
            }
            this.main = main;
            txtContent.Focus();
        //    btnDelete.Enabled = false;

           // FormMain.lvQuestionClick = false;
        }

        //Lấy tên file từ FormMain
        string FileName = FormMain.FileName;

        //Lấy tên file open từ FormMain
        string FileNameOpen = FormMain.FileNameOpen;
        //Các biến lưu đường dẫn của media
      //  string PictureURL = "";
    //    string AudioURL = "";
    //    string VideoURL = "";
        
        //lấy vị trí phương án được chọn trên lvChoice
        int choiceSelected;

        //lấy tên phương án cuối cùng câu hỏi được chọn trên ListQuestion. VD: lastIndexChoiceQuestionLoad=66-->tên phương án cuối là B
        //dùng khi thêm phương án mới vào cho ListChoice. Khi thêm phương án mới vào thì lastIndexChoiceQuestionLoad=67-->phương án thêm vào là C
        int lastIndexChoiceQuestionLoad;

        //chuỗi đáp án
        string ans = "";

        //tên phương án bắt đầu. 65 là chữ A
        int indexAdd = 65;

        //check người dùng click nút xóa
        bool xoa = false;

        //Reset lại các item trên ucMultiChoice
        public void Reset()
        {
            txtContent.ResetText();
            txtChoice.ResetText();
       //     PictureURL = "";
         //   AudioURL = "";
          //  VideoURL = "";
            btnViewAudio.Enabled = false;
            btnViewPicture.Enabled = false;
            btnViewVideo.Enabled = false;
            btnDeleteAudio.Enabled = false;
            btnDeletePicture.Enabled = false;
            btnDeleteVideo.Enabled = false;
            txtAudio.ResetText();
            txtPicture.ResetText();
            txtVideo.ResetText();
            if(FormMain.lvQuestionClick)
                btnDelete.Enabled = true;
            else
                btnDelete.Enabled = false;
            
            btnEditChoice.Enabled = false;
            btnDeleteChoice.Enabled = false;
            btnAddChoice.Enabled = true;
            lvChoice.Items.Clear();

            txtContent.Focus();

            xoa = false;
            indexAdd = 65;
            lastIndexChoiceQuestionLoad = 0;
            ans = "";
            choiceSelected = 0;
        }

        //ghi đến file nếu thêm câu hỏi
        private void WriteToFile_Item_Add(string fileName)
        {
            //Load file xml để lưu câu hỏi được thêm vào
            FormMain.xdoc.Load(fileName);

            //tạo thẻ <Question> chứa thông tin câu hỏi
            XmlElement question = FormMain.xdoc.CreateElement("Question");
            FormMain.xdoc.DocumentElement.PrependChild(question);
            //thêm thẻ <Question> vào trong thẻ <Thread>
            FormMain.xdoc.ChildNodes.Item(0).AppendChild(question);

            // tạo thẻ <ID> chứa ID câu hỏi
            XmlElement id = FormMain.xdoc.CreateElement("ID");
            XmlText idText = FormMain.xdoc.CreateTextNode(Guid.NewGuid().ToString());
            id.AppendChild(idText);
            //thêm thẻ <ID> vào thẻ <Question>
            question.PrependChild(id);

            // tạo thẻ <Type> chứa loại câu hỏi
            XmlElement type = FormMain.xdoc.CreateElement("Type");
            XmlText typeText = FormMain.xdoc.CreateTextNode("MultiChoice");
            type.AppendChild(typeText);
            //thêm thẻ <Type> vào thẻ <Question> sau thẻ <ID>
            question.InsertAfter(type, id);

            // tạo thẻ <Content> nội dung câu hỏi
            XmlElement content = FormMain.xdoc.CreateElement("Content");
            XmlText contentText = FormMain.xdoc.CreateTextNode(txtContent.Text.ToString().Trim());
            content.AppendChild(contentText);
            //thêm thẻ <Content> vào thẻ <Question> sau thẻ <Type>
            question.InsertAfter(content, type);


            // tạo thẻ <Choices> chứa mảng các phương án
            XmlElement choices = FormMain.xdoc.CreateElement("Choices");
            //thêm thẻ <Choices> vào thẻ <Question> sau thẻ <Content>
            question.InsertAfter(choices, content);

            for (int i = lvChoice.Items.Count - 1; i >= 0; i--)
            {
                // tạo thẻ <Choice> chứa phương án
                XmlElement choice = FormMain.xdoc.CreateElement("Choice");
                XmlText choiceText = FormMain.xdoc.CreateTextNode(lvChoice.Items[i].Text);
                choice.AppendChild(choiceText);
                choices.PrependChild(choice);

                if (lvChoice.Items[i].Checked)
                    ans += lvChoice.Items[i].SubItems[0].Text + "*";

            }


            // tạo thẻ <Choices1> chứa mảng các phương án 2 dùng cho câu hỏi matching
            XmlElement choices1 = FormMain.xdoc.CreateElement("Choices1");
            //thêm thẻ <Choices1> vào thẻ <Question> sau thẻ <Choices>
            question.InsertAfter(choices1, choices);


            // tạo thẻ <Choice> trong thẻ <Choices1> chứa cột phương án 2 của câu hỏi matching
            XmlElement choice1 = FormMain.xdoc.CreateElement("Choice");
            choices1.PrependChild(choice1);
           

            // tạo thẻ <Answer> chứa đáp án
            XmlElement answer = FormMain.xdoc.CreateElement("Answer");

            XmlText answerText = FormMain.xdoc.CreateTextNode(ans);
            answer.AppendChild(answerText);
            //thêm thẻ <Answer> vào thẻ <Question> sau thẻ <Choices1>
            question.InsertAfter(answer, choices1);

            // tạo thẻ <Picture> chứa ảnh của câu hỏi
            XmlElement picture = FormMain.xdoc.CreateElement("Picture");
            XmlText pictureText = FormMain.xdoc.CreateTextNode(txtPicture.Text.ToString().Trim().Equals("") ? "" : UserPicture.ImageToBase64(txtPicture.Text.ToString().Trim()));
            picture.AppendChild(pictureText);
            //thêm thẻ <Picture> vào thẻ <Question> sau thẻ <Answer>
            question.InsertAfter(picture, answer);

            // tạo thẻ <Audio> chứa đường dẫn audio của câu hỏi
            XmlElement audio = FormMain.xdoc.CreateElement("Audio");
            XmlText audioText = FormMain.xdoc.CreateTextNode(txtAudio.Text.ToString().Trim());
            audio.AppendChild(audioText);
            //thêm thẻ <Audio> vào thẻ <Question> sau thẻ <Picture>
            question.InsertAfter(audio, picture);

            // tạo thẻ <Video> chứa đường dẫn video của câu hỏi
            XmlElement video = FormMain.xdoc.CreateElement("Video");
            XmlText videoText = FormMain.xdoc.CreateTextNode(txtVideo.Text.ToString().Trim());
            video.AppendChild(videoText);
            //thêm thẻ <Video> vào thẻ <Question> sau thẻ <Audio>
            question.InsertAfter(video, audio);

            //Lưu nội dung câu hỏi được thêm đến file
            FormMain.xdoc.Save(fileName);
        }

        //ghi đến file nếu sửa câu hỏi
        private void WriteToFile_Item_Update(Question q, XmlDocument xdocument, string fileName)
        {
            // load nội dung file xml được import
            xdocument.Load(fileName);

            // tạo thẻ <Question>
            XmlElement question = xdocument.CreateElement("Question");
            xdocument.DocumentElement.PrependChild(question);
            xdocument.ChildNodes.Item(0).AppendChild(question);

            // tạo thẻ <ID>
            XmlElement id = xdocument.CreateElement("ID");
            XmlText idText = xdocument.CreateTextNode(Guid.NewGuid().ToString());
            id.AppendChild(idText);
            question.PrependChild(id);

            // tạo thẻ <Type>
            XmlElement type = xdocument.CreateElement("Type");
            XmlText typeText = xdocument.CreateTextNode(q._Type);
            type.AppendChild(typeText);
            question.InsertAfter(type, id);

            // tạo thẻ <Content>
            XmlElement content = xdocument.CreateElement("Content");
            XmlText contentText = xdocument.CreateTextNode(q.Content);
            content.AppendChild(contentText);
            question.InsertAfter(content, type);


            // tạo thẻ <Choices>
            XmlElement choices = xdocument.CreateElement("Choices");
            question.InsertAfter(choices, content);


            for (int i = q.Choices.Count() - 1; i >= 0; i--)
            {
                // tạo thẻ <Choice>
                XmlElement choice = xdocument.CreateElement("Choice");
                XmlText choiceText = xdocument.CreateTextNode(q.Choices[i]._Choice);
                choice.AppendChild(choiceText);
                choices.PrependChild(choice);
            }

            // tạo thẻ <Choices1> cho câu hỏi Matching
            XmlElement choices1 = xdocument.CreateElement("Choices1");
            question.InsertAfter(choices1, choices);

            // tạo thẻ <Choice> trong thẻ <Choices1>
            XmlElement choice1 = xdocument.CreateElement("Choice");
            choices1.PrependChild(choice1);


            // tạo thẻ <Answer>
            XmlElement answer = xdocument.CreateElement("Answer");
            XmlText answerText = xdocument.CreateTextNode(q.Answer);
            answer.AppendChild(answerText);
            question.InsertAfter(answer, choices1);

            // tạo thẻ <Picture>
            XmlElement picture = xdocument.CreateElement("Picture");
            XmlText pictureText = xdocument.CreateTextNode(q.Picture);
            picture.AppendChild(pictureText);
            question.InsertAfter(picture, answer);

            // tạo thẻ <Audio>
            XmlElement audio = xdocument.CreateElement("Audio");
            XmlText audioText = xdocument.CreateTextNode(q.Audio);
            audio.AppendChild(audioText);
            question.InsertAfter(audio, picture);

            // tạo thẻ <Video>
            XmlElement video = xdocument.CreateElement("Video");
            XmlText videoText = xdocument.CreateTextNode(q.Video);
            video.AppendChild(videoText);
            question.InsertAfter(video, audio);

            xdocument.Load(fileName);
        }

        //ghi lại toàn bộ file xml khi sửa hoặc xóa 1 câu hỏi
        public void WriteToFile_Update(string fileName, List<Question> thre)
        {
            //update nội dung câu hỏi đã sửa
            if(!xoa)
                updateQuestion();

            //Tạo 1 file xml mới
            XmlDocument xdocument = new XmlDocument();

            //tạo declaration
            xdocument.CreateXmlDeclaration("1.0", "utf-8", null);
            XmlNode root = xdocument.CreateNode(XmlNodeType.XmlDeclaration, "", "");

            //tạo node Thread
            XmlElement rootElement = xdocument.CreateElement("Thread");

            //thêm node đến document
            xdocument.AppendChild(rootElement);
            xdocument.Save(fileName);

            //Load nội dung tất cả câu hỏi vào lại file xml
            foreach (var q in thre)
            {
                WriteToFile_Item_Update(q, xdocument, fileName);
            }
        }

        //Xử lý nút Lưu
        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtContent.Text == "" || lvChoice.Items.Count == 0)
            {
                MessageBox.Show("Vui lòng nhập đầy đủ thông tin trước khi lưu", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                //kiểm tra ListChoice có bao nhiêu phương án được chọn
                int t = 0;

                for (int i = 0; i < lvChoice.Items.Count; i++)
                {
                    if (lvChoice.Items[i].Checked)
                        t++;
                }

                
                if (t < 2)
                {
                    MessageBox.Show("Vui lòng chọn ít nhất 2 phương án đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    //user click nút import mà không click nút new trước. Sau đó user click câu hỏi trên ListQuestion
                    if (FormMain.tabName == "View" && FormMain.lvQuestionClick && !FormMain.newClick)
                    {
                        WriteToFile_Update(FileNameOpen, FormMain.thread);
                        main.ReadXmlFile(FileNameOpen);
                        main.ListQuestionUpdate(FormMain.thread);
                        
                    }
                    else
                    {
                        //nếu user click nút new. Sau đó click câu hỏi trên ListQuestion. Câu hỏi đã tồn tại trên ListQuestion
                        if (FormMain.tabName == "Add" && FormMain.lvQuestionClick && FormMain.newClick)
                        {
                            WriteToFile_Item_Add(FileName);
                            main.ReadXmlFile(FileName);
                            main.ListQuestionUpdate(FormMain.thread);
                        }
                            //user thêm câu hỏi chưa tồn tại vào ListQuestion
                        else
                        {
                            WriteToFile_Item_Add(FileName);
                            main.ReadXmlFile(FileName);
                            main.ListQuestionUpdate(FormMain.thread);
                        }
                    }
                    Reset();
                }
            }
        }
        //Xử lý nút xóa
        private void btnDelete_Click(object sender, EventArgs e)
        {
            //nếu user xóa câu hỏi  đã tồn tại trên ListView
            if ((FormMain.tabName == "View" || FormMain.tabName == "Add") && FormMain.lvQuestionClick)
            {
                xoa = true;
                FormMain.thread.RemoveAt(FormMain.currentQuestion);
                if (FormMain.newClick)
                {
                    WriteToFile_Update(FileName, FormMain.thread);
                    main.ReadXmlFile(FileName);
                    main.ListQuestionUpdate(FormMain.thread);
                }
                else
                {
                    WriteToFile_Update(FileNameOpen, FormMain.thread);
                    main.ReadXmlFile(FileNameOpen);
                    main.ListQuestionUpdate(FormMain.thread);

                }

                Reset();
            }
            else
            {
                MessageBox.Show("Câu trả hỏi chưa được lưu. Không thể xóa", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
/*
        //Upload hình ảnh
        public void UploadPicture()
        {
            try
            {
                string path = Application.StartupPath.Substring(0, Application.StartupPath.Length - 10);
                string filename = Path.GetFileName(txtPicture.Text);
                File.Copy(txtPicture.Text, path + "\\Pictures\\" + filename);
                //pictureBox.ImageLocation = path + "\\Pictures\\" + filename;
                MessageBox.Show("Đã xong");
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi upload hình !", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //Upload âm thanh
        public void UploadAudio()
        {
            try
            {
                string path = Application.StartupPath.Substring(0, Application.StartupPath.Length - 10);
                string filename = Path.GetFileName(txtAudio.Text);
                File.Copy(txtAudio.Text, path + "\\Audios\\" + filename);
                //axAudio.URL = path + "\\Audios\\" + filename;
                MessageBox.Show("Đã xong");
            }
            catch (Exception)
            {
                MessageBox.Show("Lỗi upload audio !", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //pload video
        public void UploadVideo()
        {
            try
            {
                string path = Application.StartupPath.Substring(0, Application.StartupPath.Length - 10);
                string filename = Path.GetFileName(txtVideo.Text);
                File.Copy(txtVideo.Text, path + "\\Videos\\" + filename);
                //axVideo.URL = path + "\\Videos\\" + filename;
                MessageBox.Show("Đã xong");
            }
            catch
            {
                MessageBox.Show("Lỗi upload video !", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
 * */

        //Chọn hình ảnh
        private void btnChoosePicture_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog ofd = new OpenFileDialog();
                ofd.Filter = "Chọn hình ảnh (*.jpg)|*.jpg*";
                ofd.ShowDialog();
                string fileName = ofd.FileName;
             //   PictureURL = fileName;
                if (string.IsNullOrEmpty(fileName))
                    return;
                txtPicture.Text = fileName;
                btnChoosePicture.BackColor = Color.Red;
                btnViewPicture.Enabled = true;
                btnDeletePicture.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Vui lòng chọn file ảnh nhé !");
            }
        }

        //Chọn âm thanh
        private void btnChooseAudio_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Chọn âm thanh (*.mp3)|*.mp3*";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                txtAudio.Text = ofd.FileName;
               // AudioURL = ofd.FileName;
                btnViewAudio.Enabled = true;
                btnDeleteAudio.Enabled = true;
            }
        }

        //Chọn video
        private void btnChooseVideo_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.Filter = "Chọn video (*.mp4*)|*.mp4*";

            if (ofd.ShowDialog() == DialogResult.OK)
            {
                txtVideo.Text = ofd.FileName;
               // VideoURL = ofd.FileName;
                btnViewVideo.Enabled = true;
                btnDeleteVideo.Enabled = true;
            }
        }

        //Xem hình
        private void btnViewPicture_Click(object sender, EventArgs e)
        {
            try
            {
                Picture media = new Picture();
                media.ShowPicture(txtPicture.Text.ToString().Trim());
                btnDeletePicture.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Không tìm thấy file hoặc file sai định dạng!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Nghe âm thanh
        private void btnViewAudio_Click(object sender, EventArgs e)
        {
            try
            {
                Audio audio = new Audio();
                audio.ShowAudio(txtAudio.Text.ToString().Trim());
                audio.Show();
                btnDeleteAudio.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Không tìm thấy file hoặc file sai định dạng!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Xem video
        private void btnViewVideo_Click(object sender, EventArgs e)
        {
            try
            {
                Video video = new Video();
                video.ShowVideo(txtVideo.Text.ToString().Trim());
                video.Show();
                btnDeleteVideo.Enabled = true;
            }
            catch
            {
                MessageBox.Show("Không tìm thấy file hoặc file sai định dạng!", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        //Xóa hình
        private void btnDeletePicture_Click(object sender, EventArgs e)
        {
            txtPicture.ResetText();
            btnViewPicture.Enabled = false;
            btnDeletePicture.Enabled = false;
        }

        //Xóa âm thanh
        private void btnDeleteAudio_Click(object sender, EventArgs e)
        {
            txtAudio.ResetText();
            btnViewAudio.Enabled = false;
            btnDeleteAudio.Enabled = false;
        }

        //Xóa video
        private void btnDeleteVideo_Click(object sender, EventArgs e)
        {
            txtVideo.ResetText();
            btnViewVideo.Enabled = false;
            btnDeleteVideo.Enabled = false;
        }

        //xử lý nút Thêm phương án
        
        private void btnAddChoice_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtChoice.Text!="")
                {
                    
                    //nếu sửa câu hỏi
                    if(FormMain.tabName == "View")
                    {
                        bool existChoice = false;
                        for (int i = 0; i < lvChoice.Items.Count; i++)
                        {
                            string str = lvChoice.Items[i].SubItems[0].Text;
                            if(str.Substring(3, str.Length-3) == txtChoice.Text.Trim())
                            {
                                existChoice = true;
                                break;
                            }
                            
                        }
                        if(existChoice)
                        {
                            MessageBox.Show("Phương án này đã có rồi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            lastIndexChoiceQuestionLoad++;
                            lvChoice.Items.Add(Convert.ToChar(lastIndexChoiceQuestionLoad).ToString() + ". " + txtChoice.Text.ToString());
                        }
                    }
                    //nếu thêm câu hỏi
                    else
                    {
                        bool existChoice = false;
                        for (int i = 0; i < lvChoice.Items.Count; i++)
                        {
                            
                            string str = lvChoice.Items[i].SubItems[0].Text;
                            if (str.Substring(3, str.Length - 3) == txtChoice.Text.Trim())
                            {
                                existChoice = true;
                                break;
                            }
                        }

                        if (existChoice)
                        {
                            MessageBox.Show("Phương án này đã có rồi", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            lvChoice.Items.Add(Convert.ToChar(indexAdd).ToString() + ". " + txtChoice.Text.ToString());
                            indexAdd++;
                        }
                        
                    }
                    
                    txtChoice.ResetText();
                    txtChoice.Focus();
                    
                }
                else
                    MessageBox.Show("Vui lòng nhập nội dung phương án !","Thông báo",MessageBoxButtons.OK,MessageBoxIcon.Information);
            }
            catch
            {
                MessageBox.Show("Thêm phương án không thành công !", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            
        }

        //xử lý nút Sửa phương án
        private void btnEditChoice_Click(object sender, EventArgs e)
        {
            lvChoice.Items[choiceSelected].Text = txtChoice.Text.ToString();
            int index = 65;

            for(int i=0; i<lvChoice.Items.Count; i++)
            {
                if (i != choiceSelected)
                {

                    lvChoice.Items[i].SubItems[0].Text = Convert.ToChar(index).ToString() + ". " +
                        lvChoice.Items[i].SubItems[0].Text.Substring(3, lvChoice.Items[i].SubItems[0].Text.Length - 3);
                    index++;
                }
                else
                {
                    lvChoice.Items[i].SubItems[0].Text = Convert.ToChar(index).ToString() + ". " + txtChoice.Text.Trim();
                    index++;
                }
            }
            lastIndexChoiceQuestionLoad = index - 1;
        }

        //xử lý nút Xóa phương án
        private void btnDeleteChoice_Click(object sender, EventArgs e)
        {
            lvChoice.Items.RemoveAt(choiceSelected);
            int index = 65;

            for(int i=0; i<lvChoice.Items.Count; i++)
            {

                lvChoice.Items[i].SubItems[0].Text = Convert.ToChar(index).ToString() + ". " + 
                    lvChoice.Items[i].SubItems[0].Text.Substring(3, lvChoice.Items[i].SubItems[0].Text.Length-3);
                index++;
            }
            lastIndexChoiceQuestionLoad = index - 1;
            txtChoice.ResetText();
            txtChoice.Focus();
        }


        //Xử lý khi click item trên list phương án
        private void lvChoice_Click(object sender, EventArgs e)
        {
            try
            {
                choiceSelected = lvChoice.Items.IndexOf(lvChoice.SelectedItems[0]);
                txtChoice.Text = lvChoice.SelectedItems[0].SubItems[0].Text.Substring(3, lvChoice.SelectedItems[0].SubItems[0].Text.Length - 3);

                btnEditChoice.Enabled = true;
                btnDeleteChoice.Enabled = true;
            }
            catch
            {
                //MessageBox.Show("Chọn phương án trước khi chọn câu trả lời đúng", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        //Load dữ liệu vào các item khi click câu hỏi trên list câu hỏi - dùng để sửa
        public void LoadData()
        {
            
            txtContent.Text = FormMain.thread[FormMain.currentQuestion].Content;
            List<string> answer = FormMain.thread[FormMain.currentQuestion].Answer.Split('*').ToList();
            for (int i = 0; i < FormMain.thread[FormMain.currentQuestion].Choices.Count(); i++ )
            {
                lvChoice.Items.Add(FormMain.thread[FormMain.currentQuestion].Choices[i]._Choice);
                for(int j=0; j<answer.Count(); j++)
                {
                    if(FormMain.thread[FormMain.currentQuestion].Choices[i]._Choice == answer[j])
                    {
                        lvChoice.Items[i].Checked = true;
                    }
                }
            }
            
            txtPicture.Text = FormMain.thread[FormMain.currentQuestion].Picture;
            txtAudio.Text = FormMain.thread[FormMain.currentQuestion].Audio;
            txtVideo.Text = FormMain.thread[FormMain.currentQuestion].Video;

            if (txtPicture.Text.Trim() != "")
            {
                btnViewPicture.Enabled = true;
                btnDeletePicture.Enabled = true;
            }
            if (txtAudio.Text.Trim() != "")
            {
                btnViewAudio.Enabled = true;
                btnDeleteAudio.Enabled = true;
            }
            if (txtVideo.Text.Trim() != "")
            {
                btnViewVideo.Enabled = true;
                btnDeleteVideo.Enabled = true;
            }

            int lastIndex = FormMain.thread[FormMain.currentQuestion].Choices.Count() - 1;
            lastIndexChoiceQuestionLoad = Convert.ToInt32(Convert.ToChar((FormMain.thread[FormMain.currentQuestion].Choices[lastIndex]._Choice).Substring(0, 1)));

        }
        
        //cập nhật nội dung câu hỏi khi sửa
        public void updateQuestion()
        {
            //xoa = false;
            FormMain.thread[FormMain.currentQuestion].Content = txtContent.Text;
            FormMain.thread[FormMain.currentQuestion].Choices.Clear();
            for (int i = 0; i < lvChoice.Items.Count; i++)
            {
                FormMain.thread[FormMain.currentQuestion].Choices.Add(new Choice(lvChoice.Items[i].SubItems[0].Text.ToString()));

                if(lvChoice.Items[i].Checked)
                {
                    ans += lvChoice.Items[i].SubItems[0].Text + "*";
                }
            }

            FormMain.thread[FormMain.currentQuestion].Answer = ans;
            FormMain.thread[FormMain.currentQuestion].Picture = txtPicture.Text;
            FormMain.thread[FormMain.currentQuestion].Audio = txtAudio.Text;
            FormMain.thread[FormMain.currentQuestion].Video = txtVideo.Text;
            xoa = false;
        }
    }
}
