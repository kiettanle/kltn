﻿namespace ThreadComposer
{
    partial class ucSingleChoice
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucSingleChoice));
            this.groupControlSingleChoice = new DevExpress.XtraEditors.GroupControl();
            this.btnDeleteChoice = new DevExpress.XtraEditors.SimpleButton();
            this.btnEditChoice = new DevExpress.XtraEditors.SimpleButton();
            this.btnAddChoice = new DevExpress.XtraEditors.SimpleButton();
            this.lvChoice = new System.Windows.Forms.ListView();
            this.colChoice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label7 = new System.Windows.Forms.Label();
            this.txtChoice = new System.Windows.Forms.RichTextBox();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.txtAnswer = new System.Windows.Forms.RichTextBox();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnDeleteVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteAudio = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeletePicture = new DevExpress.XtraEditors.SimpleButton();
            this.btnViewVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btnViewAudio = new DevExpress.XtraEditors.SimpleButton();
            this.btnViewPicture = new DevExpress.XtraEditors.SimpleButton();
            this.txtAudio = new System.Windows.Forms.TextBox();
            this.txtVideo = new System.Windows.Forms.TextBox();
            this.txtPicture = new System.Windows.Forms.TextBox();
            this.btnChooseVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btnChooseAudio = new DevExpress.XtraEditors.SimpleButton();
            this.btnChoosePicture = new DevExpress.XtraEditors.SimpleButton();
            this.label5 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtContent = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlSingleChoice)).BeginInit();
            this.groupControlSingleChoice.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControlSingleChoice
            // 
            this.groupControlSingleChoice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControlSingleChoice.Controls.Add(this.btnDeleteChoice);
            this.groupControlSingleChoice.Controls.Add(this.btnEditChoice);
            this.groupControlSingleChoice.Controls.Add(this.btnAddChoice);
            this.groupControlSingleChoice.Controls.Add(this.lvChoice);
            this.groupControlSingleChoice.Controls.Add(this.label7);
            this.groupControlSingleChoice.Controls.Add(this.txtChoice);
            this.groupControlSingleChoice.Controls.Add(this.btnSave);
            this.groupControlSingleChoice.Controls.Add(this.txtAnswer);
            this.groupControlSingleChoice.Controls.Add(this.btnDelete);
            this.groupControlSingleChoice.Controls.Add(this.groupBox2);
            this.groupControlSingleChoice.Controls.Add(this.label5);
            this.groupControlSingleChoice.Controls.Add(this.label8);
            this.groupControlSingleChoice.Controls.Add(this.txtContent);
            this.groupControlSingleChoice.Location = new System.Drawing.Point(0, 0);
            this.groupControlSingleChoice.Name = "groupControlSingleChoice";
            this.groupControlSingleChoice.Size = new System.Drawing.Size(1062, 535);
            this.groupControlSingleChoice.TabIndex = 35;
            this.groupControlSingleChoice.Text = "Câu hỏi một lựa chọn";
            // 
            // btnDeleteChoice
            // 
            this.btnDeleteChoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteChoice.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteChoice.Image")));
            this.btnDeleteChoice.Location = new System.Drawing.Point(974, 101);
            this.btnDeleteChoice.Name = "btnDeleteChoice";
            this.btnDeleteChoice.Size = new System.Drawing.Size(75, 23);
            this.btnDeleteChoice.TabIndex = 73;
            this.btnDeleteChoice.Text = "Xóa";
            this.btnDeleteChoice.Click += new System.EventHandler(this.btnDeleteChoice_Click);
            // 
            // btnEditChoice
            // 
            this.btnEditChoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnEditChoice.Image = ((System.Drawing.Image)(resources.GetObject("btnEditChoice.Image")));
            this.btnEditChoice.Location = new System.Drawing.Point(893, 101);
            this.btnEditChoice.Name = "btnEditChoice";
            this.btnEditChoice.Size = new System.Drawing.Size(75, 23);
            this.btnEditChoice.TabIndex = 72;
            this.btnEditChoice.Text = "Sửa";
            this.btnEditChoice.Click += new System.EventHandler(this.btnEditChoice_Click);
            // 
            // btnAddChoice
            // 
            this.btnAddChoice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddChoice.Image = ((System.Drawing.Image)(resources.GetObject("btnAddChoice.Image")));
            this.btnAddChoice.Location = new System.Drawing.Point(812, 101);
            this.btnAddChoice.Name = "btnAddChoice";
            this.btnAddChoice.Size = new System.Drawing.Size(75, 23);
            this.btnAddChoice.TabIndex = 71;
            this.btnAddChoice.Text = "Thêm";
            this.btnAddChoice.Click += new System.EventHandler(this.btnAddChoice_Click);
            // 
            // lvChoice
            // 
            this.lvChoice.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colChoice});
            this.lvChoice.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.lvChoice.FullRowSelect = true;
            this.lvChoice.GridLines = true;
            this.lvChoice.Location = new System.Drawing.Point(115, 136);
            this.lvChoice.Name = "lvChoice";
            this.lvChoice.Size = new System.Drawing.Size(932, 169);
            this.lvChoice.TabIndex = 70;
            this.lvChoice.UseCompatibleStateImageBehavior = false;
            this.lvChoice.View = System.Windows.Forms.View.Details;
            this.lvChoice.Click += new System.EventHandler(this.lvChoice_Click);
            // 
            // colChoice
            // 
            this.colChoice.Text = "Phương án";
            this.colChoice.Width = 737;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label7.Location = new System.Drawing.Point(36, 106);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(73, 13);
            this.label7.TabIndex = 69;
            this.label7.Text = "Phương án :";
            // 
            // txtChoice
            // 
            this.txtChoice.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtChoice.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtChoice.Location = new System.Drawing.Point(115, 96);
            this.txtChoice.Name = "txtChoice";
            this.txtChoice.Size = new System.Drawing.Size(686, 34);
            this.txtChoice.TabIndex = 68;
            this.txtChoice.Text = "";
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(420, 489);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 65;
            this.btnSave.Text = "Lưu";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtAnswer
            // 
            this.txtAnswer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAnswer.Location = new System.Drawing.Point(115, 327);
            this.txtAnswer.Name = "txtAnswer";
            this.txtAnswer.Size = new System.Drawing.Size(103, 22);
            this.txtAnswer.TabIndex = 48;
            this.txtAnswer.Text = "";
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(582, 489);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 66;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnDeleteVideo);
            this.groupBox2.Controls.Add(this.btnDeleteAudio);
            this.groupBox2.Controls.Add(this.btnDeletePicture);
            this.groupBox2.Controls.Add(this.btnViewVideo);
            this.groupBox2.Controls.Add(this.btnViewAudio);
            this.groupBox2.Controls.Add(this.btnViewPicture);
            this.groupBox2.Controls.Add(this.txtAudio);
            this.groupBox2.Controls.Add(this.txtVideo);
            this.groupBox2.Controls.Add(this.txtPicture);
            this.groupBox2.Controls.Add(this.btnChooseVideo);
            this.groupBox2.Controls.Add(this.btnChooseAudio);
            this.groupBox2.Controls.Add(this.btnChoosePicture);
            this.groupBox2.Location = new System.Drawing.Point(12, 353);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1042, 109);
            this.groupBox2.TabIndex = 46;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Media";
            // 
            // btnDeleteVideo
            // 
            this.btnDeleteVideo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteVideo.Enabled = false;
            this.btnDeleteVideo.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteVideo.Image")));
            this.btnDeleteVideo.Location = new System.Drawing.Point(1012, 76);
            this.btnDeleteVideo.Name = "btnDeleteVideo";
            this.btnDeleteVideo.Size = new System.Drawing.Size(23, 23);
            this.btnDeleteVideo.TabIndex = 28;
            this.btnDeleteVideo.Click += new System.EventHandler(this.btnDeleteVideo_Click);
            // 
            // btnDeleteAudio
            // 
            this.btnDeleteAudio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteAudio.Enabled = false;
            this.btnDeleteAudio.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteAudio.Image")));
            this.btnDeleteAudio.Location = new System.Drawing.Point(1012, 47);
            this.btnDeleteAudio.Name = "btnDeleteAudio";
            this.btnDeleteAudio.Size = new System.Drawing.Size(23, 23);
            this.btnDeleteAudio.TabIndex = 27;
            this.btnDeleteAudio.Click += new System.EventHandler(this.btnDeleteAudio_Click);
            // 
            // btnDeletePicture
            // 
            this.btnDeletePicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeletePicture.Enabled = false;
            this.btnDeletePicture.Image = ((System.Drawing.Image)(resources.GetObject("btnDeletePicture.Image")));
            this.btnDeletePicture.Location = new System.Drawing.Point(1012, 18);
            this.btnDeletePicture.Name = "btnDeletePicture";
            this.btnDeletePicture.Size = new System.Drawing.Size(23, 23);
            this.btnDeletePicture.TabIndex = 26;
            this.btnDeletePicture.Click += new System.EventHandler(this.btnDeletePicture_Click);
            // 
            // btnViewVideo
            // 
            this.btnViewVideo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewVideo.Enabled = false;
            this.btnViewVideo.Image = ((System.Drawing.Image)(resources.GetObject("btnViewVideo.Image")));
            this.btnViewVideo.Location = new System.Drawing.Point(983, 76);
            this.btnViewVideo.Name = "btnViewVideo";
            this.btnViewVideo.Size = new System.Drawing.Size(23, 23);
            this.btnViewVideo.TabIndex = 25;
            this.btnViewVideo.Click += new System.EventHandler(this.btnViewVideo_Click);
            // 
            // btnViewAudio
            // 
            this.btnViewAudio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewAudio.Enabled = false;
            this.btnViewAudio.Image = ((System.Drawing.Image)(resources.GetObject("btnViewAudio.Image")));
            this.btnViewAudio.Location = new System.Drawing.Point(983, 47);
            this.btnViewAudio.Name = "btnViewAudio";
            this.btnViewAudio.Size = new System.Drawing.Size(23, 23);
            this.btnViewAudio.TabIndex = 24;
            this.btnViewAudio.Click += new System.EventHandler(this.btnViewAudio_Click);
            // 
            // btnViewPicture
            // 
            this.btnViewPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewPicture.Enabled = false;
            this.btnViewPicture.Image = ((System.Drawing.Image)(resources.GetObject("btnViewPicture.Image")));
            this.btnViewPicture.Location = new System.Drawing.Point(983, 18);
            this.btnViewPicture.Name = "btnViewPicture";
            this.btnViewPicture.Size = new System.Drawing.Size(23, 23);
            this.btnViewPicture.TabIndex = 23;
            this.btnViewPicture.Click += new System.EventHandler(this.btnViewPicture_Click);
            // 
            // txtAudio
            // 
            this.txtAudio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAudio.Location = new System.Drawing.Point(103, 49);
            this.txtAudio.Name = "txtAudio";
            this.txtAudio.Size = new System.Drawing.Size(874, 21);
            this.txtAudio.TabIndex = 22;
            // 
            // txtVideo
            // 
            this.txtVideo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVideo.Location = new System.Drawing.Point(103, 78);
            this.txtVideo.Name = "txtVideo";
            this.txtVideo.Size = new System.Drawing.Size(874, 21);
            this.txtVideo.TabIndex = 21;
            // 
            // txtPicture
            // 
            this.txtPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPicture.Location = new System.Drawing.Point(103, 18);
            this.txtPicture.Name = "txtPicture";
            this.txtPicture.Size = new System.Drawing.Size(874, 21);
            this.txtPicture.TabIndex = 19;
            // 
            // btnChooseVideo
            // 
            this.btnChooseVideo.Image = ((System.Drawing.Image)(resources.GetObject("btnChooseVideo.Image")));
            this.btnChooseVideo.Location = new System.Drawing.Point(18, 76);
            this.btnChooseVideo.Name = "btnChooseVideo";
            this.btnChooseVideo.Size = new System.Drawing.Size(75, 23);
            this.btnChooseVideo.TabIndex = 2;
            this.btnChooseVideo.Text = "Video";
            this.btnChooseVideo.Click += new System.EventHandler(this.btnChooseVideo_Click);
            // 
            // btnChooseAudio
            // 
            this.btnChooseAudio.Image = ((System.Drawing.Image)(resources.GetObject("btnChooseAudio.Image")));
            this.btnChooseAudio.Location = new System.Drawing.Point(18, 47);
            this.btnChooseAudio.Name = "btnChooseAudio";
            this.btnChooseAudio.Size = new System.Drawing.Size(75, 23);
            this.btnChooseAudio.TabIndex = 1;
            this.btnChooseAudio.Text = "Âm thanh";
            this.btnChooseAudio.Click += new System.EventHandler(this.btnChooseAudio_Click);
            // 
            // btnChoosePicture
            // 
            this.btnChoosePicture.Image = ((System.Drawing.Image)(resources.GetObject("btnChoosePicture.Image")));
            this.btnChoosePicture.Location = new System.Drawing.Point(18, 18);
            this.btnChoosePicture.Name = "btnChoosePicture";
            this.btnChoosePicture.Size = new System.Drawing.Size(75, 23);
            this.btnChoosePicture.TabIndex = 0;
            this.btnChoosePicture.Text = "Hình ảnh";
            this.btnChoosePicture.Click += new System.EventHandler(this.btnChoosePicture_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label5.Location = new System.Drawing.Point(5, 330);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(104, 13);
            this.label5.TabIndex = 41;
            this.label5.Text = "Phương án đúng :";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label8.Location = new System.Drawing.Point(5, 57);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(104, 13);
            this.label8.TabIndex = 38;
            this.label8.Text = "Nội dung câu hỏi :";
            // 
            // txtContent
            // 
            this.txtContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtContent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtContent.Location = new System.Drawing.Point(115, 30);
            this.txtContent.Name = "txtContent";
            this.txtContent.Size = new System.Drawing.Size(935, 59);
            this.txtContent.TabIndex = 35;
            this.txtContent.Text = "";
            // 
            // ucSingleChoice
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControlSingleChoice);
            this.Name = "ucSingleChoice";
            this.Size = new System.Drawing.Size(1062, 538);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlSingleChoice)).EndInit();
            this.groupControlSingleChoice.ResumeLayout(false);
            this.groupControlSingleChoice.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControlSingleChoice;
        private System.Windows.Forms.GroupBox groupBox2;
        private DevExpress.XtraEditors.SimpleButton btnDeleteVideo;
        private DevExpress.XtraEditors.SimpleButton btnDeleteAudio;
        private DevExpress.XtraEditors.SimpleButton btnDeletePicture;
        private DevExpress.XtraEditors.SimpleButton btnViewVideo;
        private DevExpress.XtraEditors.SimpleButton btnViewAudio;
        private DevExpress.XtraEditors.SimpleButton btnViewPicture;
        private System.Windows.Forms.TextBox txtAudio;
        private System.Windows.Forms.TextBox txtVideo;
        private System.Windows.Forms.TextBox txtPicture;
        private DevExpress.XtraEditors.SimpleButton btnChooseVideo;
        private DevExpress.XtraEditors.SimpleButton btnChooseAudio;
        private DevExpress.XtraEditors.SimpleButton btnChoosePicture;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.RichTextBox txtContent;
        private System.Windows.Forms.RichTextBox txtAnswer;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnDeleteChoice;
        private DevExpress.XtraEditors.SimpleButton btnEditChoice;
        private DevExpress.XtraEditors.SimpleButton btnAddChoice;
        private System.Windows.Forms.ListView lvChoice;
        private System.Windows.Forms.ColumnHeader colChoice;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.RichTextBox txtChoice;

    }
}
