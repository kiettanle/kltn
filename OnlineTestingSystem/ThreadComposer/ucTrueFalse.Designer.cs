﻿namespace ThreadComposer
{
    partial class ucTrueFalse
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucTrueFalse));
            this.groupControlTrueFalse = new DevExpress.XtraEditors.GroupControl();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnSave = new DevExpress.XtraEditors.SimpleButton();
            this.btnDelete = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeleteAudio = new DevExpress.XtraEditors.SimpleButton();
            this.btnDeletePicture = new DevExpress.XtraEditors.SimpleButton();
            this.btnViewVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btnViewAudio = new DevExpress.XtraEditors.SimpleButton();
            this.btnViewPicture = new DevExpress.XtraEditors.SimpleButton();
            this.txtAudio = new System.Windows.Forms.TextBox();
            this.txtVideo = new System.Windows.Forms.TextBox();
            this.txtPicture = new System.Windows.Forms.TextBox();
            this.btnChooseVideo = new DevExpress.XtraEditors.SimpleButton();
            this.btnChooseAudio = new DevExpress.XtraEditors.SimpleButton();
            this.btnChoosePicture = new DevExpress.XtraEditors.SimpleButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lvChoice = new System.Windows.Forms.ListView();
            this.colChoice = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAnswer = new System.Windows.Forms.RichTextBox();
            this.txtContent = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTrueFalse)).BeginInit();
            this.groupControlTrueFalse.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupControlTrueFalse
            // 
            this.groupControlTrueFalse.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.groupControlTrueFalse.Controls.Add(this.panel2);
            this.groupControlTrueFalse.Controls.Add(this.panel1);
            this.groupControlTrueFalse.Location = new System.Drawing.Point(0, 0);
            this.groupControlTrueFalse.Name = "groupControlTrueFalse";
            this.groupControlTrueFalse.Size = new System.Drawing.Size(889, 526);
            this.groupControlTrueFalse.TabIndex = 2;
            this.groupControlTrueFalse.Text = "Câu hỏi đúng sai";
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.btnSave);
            this.panel2.Controls.Add(this.btnDelete);
            this.panel2.Controls.Add(this.btnDeleteVideo);
            this.panel2.Controls.Add(this.btnDeleteAudio);
            this.panel2.Controls.Add(this.btnDeletePicture);
            this.panel2.Controls.Add(this.btnViewVideo);
            this.panel2.Controls.Add(this.btnViewAudio);
            this.panel2.Controls.Add(this.btnViewPicture);
            this.panel2.Controls.Add(this.txtAudio);
            this.panel2.Controls.Add(this.txtVideo);
            this.panel2.Controls.Add(this.txtPicture);
            this.panel2.Controls.Add(this.btnChooseVideo);
            this.panel2.Controls.Add(this.btnChooseAudio);
            this.panel2.Controls.Add(this.btnChoosePicture);
            this.panel2.Location = new System.Drawing.Point(2, 204);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(885, 128);
            this.panel2.TabIndex = 25;
            // 
            // btnSave
            // 
            this.btnSave.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSave.Image = ((System.Drawing.Image)(resources.GetObject("btnSave.Image")));
            this.btnSave.Location = new System.Drawing.Point(318, 97);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 59;
            this.btnSave.Text = "Lưu";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnDelete.Image = ((System.Drawing.Image)(resources.GetObject("btnDelete.Image")));
            this.btnDelete.Location = new System.Drawing.Point(480, 97);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 60;
            this.btnDelete.Text = "Xóa";
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnDeleteVideo
            // 
            this.btnDeleteVideo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteVideo.Enabled = false;
            this.btnDeleteVideo.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteVideo.Image")));
            this.btnDeleteVideo.Location = new System.Drawing.Point(850, 68);
            this.btnDeleteVideo.Name = "btnDeleteVideo";
            this.btnDeleteVideo.Size = new System.Drawing.Size(23, 23);
            this.btnDeleteVideo.TabIndex = 43;
            this.btnDeleteVideo.Click += new System.EventHandler(this.btnDeleteVideo_Click);
            // 
            // btnDeleteAudio
            // 
            this.btnDeleteAudio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeleteAudio.Enabled = false;
            this.btnDeleteAudio.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteAudio.Image")));
            this.btnDeleteAudio.Location = new System.Drawing.Point(850, 39);
            this.btnDeleteAudio.Name = "btnDeleteAudio";
            this.btnDeleteAudio.Size = new System.Drawing.Size(23, 23);
            this.btnDeleteAudio.TabIndex = 42;
            this.btnDeleteAudio.Click += new System.EventHandler(this.btnDeleteAudio_Click);
            // 
            // btnDeletePicture
            // 
            this.btnDeletePicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDeletePicture.Enabled = false;
            this.btnDeletePicture.Image = ((System.Drawing.Image)(resources.GetObject("btnDeletePicture.Image")));
            this.btnDeletePicture.Location = new System.Drawing.Point(850, 10);
            this.btnDeletePicture.Name = "btnDeletePicture";
            this.btnDeletePicture.Size = new System.Drawing.Size(23, 23);
            this.btnDeletePicture.TabIndex = 41;
            this.btnDeletePicture.Click += new System.EventHandler(this.btnDeletePicture_Click);
            // 
            // btnViewVideo
            // 
            this.btnViewVideo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewVideo.Enabled = false;
            this.btnViewVideo.Image = ((System.Drawing.Image)(resources.GetObject("btnViewVideo.Image")));
            this.btnViewVideo.Location = new System.Drawing.Point(821, 68);
            this.btnViewVideo.Name = "btnViewVideo";
            this.btnViewVideo.Size = new System.Drawing.Size(23, 23);
            this.btnViewVideo.TabIndex = 40;
            this.btnViewVideo.Click += new System.EventHandler(this.btnViewVideo_Click);
            // 
            // btnViewAudio
            // 
            this.btnViewAudio.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewAudio.Enabled = false;
            this.btnViewAudio.Image = ((System.Drawing.Image)(resources.GetObject("btnViewAudio.Image")));
            this.btnViewAudio.Location = new System.Drawing.Point(821, 39);
            this.btnViewAudio.Name = "btnViewAudio";
            this.btnViewAudio.Size = new System.Drawing.Size(23, 23);
            this.btnViewAudio.TabIndex = 39;
            this.btnViewAudio.Click += new System.EventHandler(this.btnViewAudio_Click);
            // 
            // btnViewPicture
            // 
            this.btnViewPicture.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnViewPicture.Enabled = false;
            this.btnViewPicture.Image = ((System.Drawing.Image)(resources.GetObject("btnViewPicture.Image")));
            this.btnViewPicture.Location = new System.Drawing.Point(821, 10);
            this.btnViewPicture.Name = "btnViewPicture";
            this.btnViewPicture.Size = new System.Drawing.Size(23, 23);
            this.btnViewPicture.TabIndex = 38;
            this.btnViewPicture.Click += new System.EventHandler(this.btnViewPicture_Click);
            // 
            // txtAudio
            // 
            this.txtAudio.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAudio.Location = new System.Drawing.Point(122, 41);
            this.txtAudio.Name = "txtAudio";
            this.txtAudio.Size = new System.Drawing.Size(693, 21);
            this.txtAudio.TabIndex = 37;
            // 
            // txtVideo
            // 
            this.txtVideo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtVideo.Location = new System.Drawing.Point(122, 70);
            this.txtVideo.Name = "txtVideo";
            this.txtVideo.Size = new System.Drawing.Size(693, 21);
            this.txtVideo.TabIndex = 36;
            // 
            // txtPicture
            // 
            this.txtPicture.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPicture.Location = new System.Drawing.Point(122, 10);
            this.txtPicture.Name = "txtPicture";
            this.txtPicture.Size = new System.Drawing.Size(693, 21);
            this.txtPicture.TabIndex = 35;
            // 
            // btnChooseVideo
            // 
            this.btnChooseVideo.Image = ((System.Drawing.Image)(resources.GetObject("btnChooseVideo.Image")));
            this.btnChooseVideo.Location = new System.Drawing.Point(37, 68);
            this.btnChooseVideo.Name = "btnChooseVideo";
            this.btnChooseVideo.Size = new System.Drawing.Size(75, 23);
            this.btnChooseVideo.TabIndex = 31;
            this.btnChooseVideo.Text = "Video";
            this.btnChooseVideo.Click += new System.EventHandler(this.btnChooseVideo_Click);
            // 
            // btnChooseAudio
            // 
            this.btnChooseAudio.Image = ((System.Drawing.Image)(resources.GetObject("btnChooseAudio.Image")));
            this.btnChooseAudio.Location = new System.Drawing.Point(37, 39);
            this.btnChooseAudio.Name = "btnChooseAudio";
            this.btnChooseAudio.Size = new System.Drawing.Size(75, 23);
            this.btnChooseAudio.TabIndex = 30;
            this.btnChooseAudio.Text = "Âm thanh";
            this.btnChooseAudio.Click += new System.EventHandler(this.btnChooseAudio_Click);
            // 
            // btnChoosePicture
            // 
            this.btnChoosePicture.Image = ((System.Drawing.Image)(resources.GetObject("btnChoosePicture.Image")));
            this.btnChoosePicture.Location = new System.Drawing.Point(37, 10);
            this.btnChoosePicture.Name = "btnChoosePicture";
            this.btnChoosePicture.Size = new System.Drawing.Size(75, 23);
            this.btnChoosePicture.TabIndex = 29;
            this.btnChoosePicture.Text = "Hình ảnh";
            this.btnChoosePicture.Click += new System.EventHandler(this.btnChoosePicture_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lvChoice);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.txtAnswer);
            this.panel1.Controls.Add(this.txtContent);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(2, 21);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(885, 213);
            this.panel1.TabIndex = 24;
            // 
            // lvChoice
            // 
            this.lvChoice.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colChoice});
            this.lvChoice.GridLines = true;
            this.lvChoice.Location = new System.Drawing.Point(122, 75);
            this.lvChoice.Name = "lvChoice";
            this.lvChoice.Size = new System.Drawing.Size(751, 75);
            this.lvChoice.TabIndex = 31;
            this.lvChoice.UseCompatibleStateImageBehavior = false;
            this.lvChoice.View = System.Windows.Forms.View.Details;
            this.lvChoice.Click += new System.EventHandler(this.lvChoice_Click);
            // 
            // colChoice
            // 
            this.colChoice.Text = "Phương án";
            this.colChoice.Width = 739;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label4.Location = new System.Drawing.Point(13, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Phương án đúng :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(33, 76);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 13);
            this.label2.TabIndex = 28;
            this.label2.Text = "Phương án:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Tahoma", 8.25F, System.Drawing.FontStyle.Bold);
            this.label1.Location = new System.Drawing.Point(13, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 13);
            this.label1.TabIndex = 27;
            this.label1.Text = "Nội dung câu hỏi :";
            // 
            // txtAnswer
            // 
            this.txtAnswer.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAnswer.Location = new System.Drawing.Point(123, 156);
            this.txtAnswer.Name = "txtAnswer";
            this.txtAnswer.Size = new System.Drawing.Size(104, 21);
            this.txtAnswer.TabIndex = 25;
            this.txtAnswer.Text = "";
            // 
            // txtContent
            // 
            this.txtContent.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtContent.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtContent.Location = new System.Drawing.Point(122, 18);
            this.txtContent.Name = "txtContent";
            this.txtContent.Size = new System.Drawing.Size(749, 40);
            this.txtContent.TabIndex = 24;
            this.txtContent.Text = "";
            // 
            // ucTrueFalse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.groupControlTrueFalse);
            this.Name = "ucTrueFalse";
            this.Size = new System.Drawing.Size(889, 526);
            ((System.ComponentModel.ISupportInitialize)(this.groupControlTrueFalse)).EndInit();
            this.groupControlTrueFalse.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraEditors.GroupControl groupControlTrueFalse;
        private System.Windows.Forms.Panel panel2;
        private DevExpress.XtraEditors.SimpleButton btnDeleteVideo;
        private DevExpress.XtraEditors.SimpleButton btnDeleteAudio;
        private DevExpress.XtraEditors.SimpleButton btnDeletePicture;
        private DevExpress.XtraEditors.SimpleButton btnViewVideo;
        private DevExpress.XtraEditors.SimpleButton btnViewAudio;
        private DevExpress.XtraEditors.SimpleButton btnViewPicture;
        private System.Windows.Forms.TextBox txtAudio;
        private System.Windows.Forms.TextBox txtVideo;
        private System.Windows.Forms.TextBox txtPicture;
        private DevExpress.XtraEditors.SimpleButton btnChooseVideo;
        private DevExpress.XtraEditors.SimpleButton btnChooseAudio;
        private DevExpress.XtraEditors.SimpleButton btnChoosePicture;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox txtAnswer;
        private System.Windows.Forms.RichTextBox txtContent;
        private DevExpress.XtraEditors.SimpleButton btnDelete;
        private DevExpress.XtraEditors.SimpleButton btnSave;
        private System.Windows.Forms.ListView lvChoice;
        private System.Windows.Forms.ColumnHeader colChoice;



    }
}
